import { createFeatureSelector, createSelector } from "@ngrx/store";

import { citiesFeatureName, ICitiesState } from "./states";

/**
 * Список городов.
 */
export const all = createSelector(
  createFeatureSelector<ICitiesState>(citiesFeatureName),
  (state) => {
    return [...(state.list ?? [])];
  }
);

/**
 * Количество городов.
 */
export const counts = createSelector(
  all,
  (cities) => {
    return cities?.length ?? 0;
  }
);

/**
 * Данные города по ID.
 * @param id ID города.
 * @returns Данные города.
 */
export const byId = (id: number) => createSelector(
  all,
  (cities) => {
    return cities?.find(x => x.id == id) ?? null;
  }
);

/**
 * Первый город из списка.
 * @returns Данные города.
 */
export const first = createSelector(
  all,
  (cities) => {
    return (cities?.length ?? 0) >= 1
      ? cities[0]
      : null;
  }
);

/**
 * Последний город из списка.
 * @returns Данные города.
 */
export const last = createSelector(
  all,
  (cities) => {
    let i = cities?.length ?? 0;
    return (i) >= 1
      ? cities[i - 1]
      : null;
  }
);

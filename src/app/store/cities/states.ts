import { CityModel } from "../../models/city-model";

export const citiesFeatureName = 'cities';

export interface ICitiesState {
  list: Array<CityModel> | null;
}

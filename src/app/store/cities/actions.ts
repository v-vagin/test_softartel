import { createAction, props } from "@ngrx/store";

import { IdModel } from "../../models/id-model";
import { CityModel } from "../../models/city-model";
import { ActionPropsModel } from "../../models/action-props-model";
import { ItemModel } from "../../models/item-model";

export enum CitiesActionEnum {
  // Actions для Store Эффектов.
  Get         = '[Cities] Get', // Получить данные с БД API сервера.
  Add         = '[Cities] Add', // Добавить данные в БД API сервера.
  Put         = '[Cities] Put', // Обновить данные в БД API сервера.
  Del         = '[Cities] Del', // Удалить данные из БД API сервера.

  // Actions для Store Редукторов.
  ApiError    = '[Cities] ApiError', // Запрос в БД API сервера завершён с ошибкой, которую необходимо обработать.
  GetOk       = '[Cities] GetOk', // Данные получены с БД API сервера и их необходимо сохранить в Store.
  AddOk       = '[Cities] AddOk', // Данные добавлены в БД API сервера и их необходимо добавить в Store.
  PutOk       = '[Cities] PutOk', // Данные обновлены в БД API сервера и их необходимо обновить в Store.
  DelOk       = '[Cities] DelOk', // Данные удалены из БД API сервера и их необходимо удалить из Store.
  Clear       = '[Cities] Clear', // Очистить данные в Store.
}

export const apiError = createAction(
  CitiesActionEnum.ApiError
);

export const get = createAction(
  CitiesActionEnum.Get
);

export const getOk = createAction(
  CitiesActionEnum.GetOk,
  props<ActionPropsModel<Array<CityModel>>>()
);

export const add = createAction(
  CitiesActionEnum.Add,
  props<ActionPropsModel<CityModel>>()
);

export const addOk = createAction(
  CitiesActionEnum.AddOk,
  props<ActionPropsModel<CityModel>>()
);

export const put = createAction(
  CitiesActionEnum.Put,
  props<ActionPropsModel<ItemModel<CityModel>>>()
);

export const putOk = createAction(
  CitiesActionEnum.PutOk,
  props<ActionPropsModel<CityModel>>()
);

export const del = createAction(
  CitiesActionEnum.Del,
  props<ActionPropsModel<IdModel>>()
);

export const delOk = createAction(
  CitiesActionEnum.DelOk,
  props<ActionPropsModel<IdModel>>()
);

export const clear = createAction(
  CitiesActionEnum.Clear
);

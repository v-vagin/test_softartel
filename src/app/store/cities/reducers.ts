import { createReducer, on } from '@ngrx/store';

import { ICitiesState } from './states';

import * as CitiesAction from './actions';
import { isIdCorrect } from '../../services/helper.service';

export const initialState: ICitiesState = {
  list: []
};

export const citiesReducer = createReducer<ICitiesState>(
  initialState,

  on(
    // Обработчик ошибки.
    CitiesAction.apiError,
    (state) => {
      return { ...state, list: structuredClone(state.list ?? []) }
    }
  ),

  on(
    // Добавить список городов в state.
    CitiesAction.getOk,
    (state, response) => {
      return { ...state, list: structuredClone(response?.data ?? []) }
    }
  ),

  on(
    // Добавить город в state.
    CitiesAction.addOk,
    (state, response) => {
      let id = response?.data?.id;
      let result = structuredClone(state.list ?? []);

      if (!isIdCorrect(id)) {
        // если город пустой, то возвращаем прежний state.
        return { ...state, list: result }
      }

      result.push(response!.data!);
      return { ...state, list: result }
    }
  ),

  on(
    // Обновить город в state.
    CitiesAction.putOk,
    (state, response) => {
      let id = response.data?.id;
      let result = structuredClone(state.list ?? []);

      if (!isIdCorrect(id)) {
        // если город пустой, то возвращаем прежний state.
        return { ...state, list: result }
      }

      let i = result.findIndex(x => (x?.id ?? 0) > 0 && x.id == response?.data?.id);

      if (i >= 0) {
        result[i] = structuredClone(response.data!);
      }

      return { ...state, list: result }
    }
  ),

  on(
    // Удалить город из state.
    CitiesAction.delOk,
    (state, response) => {
      let result = structuredClone(state.list?.filter(x => x.id != response?.data?.id) ?? []);
      return { ...state, list: result }
    }
  ),

  on(
    // Очистить список.
    CitiesAction.clear,
    (state) => {
      return { ...state, list: [] }
    }
  ),
);

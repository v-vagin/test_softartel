import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, exhaustMap, map, of } from 'rxjs';

import { ApiService } from '../../services/api.service';
import { errorToString } from '../../services/helper.service';

import { apiOk, ru_addedSuccessfully, ru_deletedSuccessfully, ru_putedSuccessfully } from '../../consts/global-const';

import { ApiListTypeEnum } from '../../enums/api-list-type-enum';
import { RequestMethodEnum } from '../../enums/request-method-enum';
import { CitiesActionEnum } from './actions';

import { CityModel } from '../../models/city-model';
import { RequestModel } from '../../models/request-model';
import { ActionPropsModel } from '../../models/action-props-model';
import { ItemModel } from '../../models/item-model';
import { EffectResponseModel } from '../../models/effect-response-model';
import { IdModel } from '../../models/id-model';

@Injectable()
export class CitiesEffects {
  constructor(
    private actions$: Actions,
    private api: ApiService,
  ) {}

  /**
   * Получить список городов.
   */
  get$ = createEffect(() => this.actions$
    .pipe(
      ofType(CitiesActionEnum.Get),
      exhaustMap(() => {
        let url = this.api.getUrl(ApiListTypeEnum.Cities);
        return this.api.request<Array<CityModel>>(url, new RequestModel()).pipe(
          map(response => {
            return new EffectResponseModel(CitiesActionEnum.GetOk, response)
          }),
          catchError((error) => {
            let message = `${CitiesActionEnum.Get}: ${errorToString(error)}`;
            this.api.notification$.next(message);
            return of(new EffectResponseModel(CitiesActionEnum.ApiError));
          })
        );
      })
    ), { dispatch: true }
  );

  /**
   * Добавить город.
   */
  add$ = createEffect(() => this.actions$
    .pipe(
      ofType(CitiesActionEnum.Add),
      exhaustMap((state: ActionPropsModel<CityModel>) => {
        let url = this.api.getUrl(ApiListTypeEnum.Cities);
        return this.api.request<CityModel>(url, new RequestModel(RequestMethodEnum.Post, state.data)).pipe(
          map(response => {
            this.api.notification$.next(ru_addedSuccessfully);
            return state.save
              ? new EffectResponseModel(CitiesActionEnum.AddOk, response)
              : new EffectResponseModel(apiOk)
          }),
          catchError((error) => {
            let message = `${CitiesActionEnum.Add}: ${errorToString(error)}`;
            this.api.notification$.next(message);
            return of(new EffectResponseModel(CitiesActionEnum.ApiError));
          })
        );
      })
    ), { dispatch: true }
  );

  /**
   * Обновить город.
   */
  put$ = createEffect(() => this.actions$
    .pipe(
      ofType(CitiesActionEnum.Put),
      exhaustMap((state: ActionPropsModel<ItemModel<CityModel>>) => {
        let url = this.api.getUrl(ApiListTypeEnum.Cities, state.data?.id);
        return this.api.request<CityModel>(url, new RequestModel(RequestMethodEnum.Put, state.data?.item)).pipe(
          map(response => {
            this.api.notification$.next(ru_putedSuccessfully);
            return state.save
              ? new EffectResponseModel(CitiesActionEnum.PutOk, response)
              : new EffectResponseModel(apiOk)
          }),
          catchError((error) => {
            let message = `${CitiesActionEnum.Put}: ${errorToString(error)}`;
            this.api.notification$.next(message);
            return of(new EffectResponseModel(CitiesActionEnum.ApiError));
          })
        )
      })
    ), { dispatch: true }
  );

  /**
   * Удалить город.
   */
  del$ = createEffect(() => this.actions$
    .pipe(
      ofType(CitiesActionEnum.Del),
      exhaustMap((state: ActionPropsModel<IdModel>) => {
        let id: number = state.data?.id ?? 0;
        let url = this.api.getUrl(ApiListTypeEnum.City, id);
        return this.api.request(url, new RequestModel(RequestMethodEnum.Delete)).pipe(
          map(() => {
            this.api.notification$.next(ru_deletedSuccessfully);
            return state.save
              ? new EffectResponseModel(CitiesActionEnum.DelOk, new IdModel(id))
              : new EffectResponseModel(apiOk)
          }),
          catchError((error) => {
            let message = `${CitiesActionEnum.Del}: ${errorToString(error)}`;
            this.api.notification$.next(message);
            return of(new EffectResponseModel(CitiesActionEnum.ApiError));
          })
        )
      })
    ), { dispatch: true }
  );

}

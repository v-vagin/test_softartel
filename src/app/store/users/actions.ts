import { createAction, props } from "@ngrx/store";

import { IdModel } from "../../models/id-model";
import { UserModel } from "../../models/user-model";
import { ActionPropsModel } from "../../models/action-props-model";
import { ItemModel } from "../../models/item-model";
import { AuthResponseModel } from "../../models/auth-response-model";

export enum UsersActionEnum {
  // Actions для Store Эффектов.
  Get         = '[Users] Get', // Получить данные с БД API сервера.
  Add         = '[Users] Add', // Добавить данные в БД API сервера.
  Put         = '[Users] Put', // Обновить данные в БД API сервера.
  Del         = '[Users] Del', // Удалить данные из БД API сервера.

  // Actions для Store Редукторов.
  ApiError    = '[Users] ApiError', // Запрос в БД API сервера завершён с ошибкой, которую необходимо обработать.
  GetOk       = '[Users] GetOk', // Данные получены с БД API сервера и их необходимо сохранить в Store.
  AddOk       = '[Users] AddOk', // Данные добавлены в БД API сервера и их необходимо добавить в Store.
  PutOk       = '[Users] PutOk', // Данные обновлены в БД API сервера и их необходимо обновить в Store.
  DelOk       = '[Users] DelOk', // Данные удалены из БД API сервера и их необходимо удалить из Store.
  Clear       = '[Users] Clear', // Очистить данные в Store.
}

export const apiError = createAction(
  UsersActionEnum.ApiError
);

export const get = createAction(
  UsersActionEnum.Get
);

export const getOk = createAction(
  UsersActionEnum.GetOk,
  props<ActionPropsModel<Array<UserModel>>>()
);

export const add = createAction(
  UsersActionEnum.Add,
  props<ActionPropsModel<UserModel>>()
);

export const addOk = createAction(
  UsersActionEnum.AddOk,
  props<ActionPropsModel<AuthResponseModel>>()
);

export const put = createAction(
  UsersActionEnum.Put,
  props<ActionPropsModel<ItemModel<UserModel>>>()
);

export const putOk = createAction(
  UsersActionEnum.PutOk,
  props<ActionPropsModel<UserModel>>()
);

export const del = createAction(
  UsersActionEnum.Del,
  props<ActionPropsModel<IdModel>>()
);

export const delOk = createAction(
  UsersActionEnum.DelOk,
  props<ActionPropsModel<IdModel>>()
);

export const clear = createAction(
  UsersActionEnum.Clear
);

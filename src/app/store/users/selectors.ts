import { createFeatureSelector, createSelector } from "@ngrx/store";

import { usersFeatureName, IUsersState } from "./states";

import * as Tickets from "../tickets/selectors";

/**
 * Список всех пользователей.
 */
export const all = createSelector(
  createFeatureSelector<IUsersState>(usersFeatureName),
  (state) => {
    return [...(state.list ?? [])];
  }
);

/**
 * Данные конкретного пользователя.
 * @param id ID пользователя.
 * @returns Данные пользователя.
 */
export const byId = (id: number | null) => createSelector(
  all,
  (users) => {
    return users?.find(x => (x?.id ?? 0) > 0 && x.id === id) ?? null;
  }
);

/**
 * Список ID тикетов конкретного пользователя.
 * @param id ID пользователя.
 * @returns Список ID тикетов.
 */
const ticketIds = (id: number) => createSelector(
  all,
  (users) => {
    return users?.find(x => x.id == id)?.tickets ?? [];
  }
);

/**
 * Количество тикетов у конкретного пользователя.
 * @param id ID пользователя.
 * @returns Список ID тикетов.
 */
export const ticketsNumber = (id: number) => createSelector(
  ticketIds(id),
  Tickets.all,
  (ticketsIds, tickets) => {
    if (id == 1) {
      return tickets?.length ?? 0;
    }

    return ticketsIds?.length ?? 0;
  }
);

/**
 * Список тикетов конкретного пользователя.
 * @param id ID пользователя.
 * @returns Список тикетов.
 */
export const tickets = (id: number) => createSelector(
  ticketIds(id),
  Tickets.all,
  (ticketIds, tickets) => {
    if (id == 1) {
      return tickets ?? [];
    }

    return tickets?.filter(x => (x?.id ?? 0) > 0 && ticketIds?.includes(x!.id!)) ?? [];
  }
);

/**
 * Первый пользователь из списка.
 * @returns Данные пользователя.
 */
export const first = createSelector(
  all,
  (users) => {
    return (users?.length ?? 0) >= 1
      ? users[0]
      : null;
  }
);

/**
 * Последний пользователь из списка.
 * @returns Данные пользователя.
 */
export const last = createSelector(
  all,
  (users) => {
    let i = users?.length ?? 0;
    return (i) >= 1
      ? users[i - 1]
      : null;
  }
);

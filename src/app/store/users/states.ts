import { UserModel } from "../../models/user-model";

export const usersFeatureName = 'users';

export interface IUsersState {
  list: Array<UserModel> | null;
}

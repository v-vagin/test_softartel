import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, exhaustMap, map, of } from 'rxjs';

import { ApiService } from '../../services/api.service';
import { errorToString } from '../../services/helper.service';

import { apiOk, ru_addedSuccessfully, ru_deletedSuccessfully, ru_putedSuccessfully } from '../../consts/global-const';

import { ApiListTypeEnum } from '../../enums/api-list-type-enum';
import { RequestMethodEnum } from '../../enums/request-method-enum';
import { UsersActionEnum } from './actions';

import { UserModel } from '../../models/user-model';
import { RequestModel } from '../../models/request-model';
import { ActionPropsModel } from '../../models/action-props-model';
import { ItemModel } from '../../models/item-model';
import { IdModel } from '../../models/id-model';
import { EffectResponseModel } from '../../models/effect-response-model';
import { AuthResponseModel } from '../../models/auth-response-model';

@Injectable()
export class UsersEffects {
  constructor(
    private actions$: Actions,
    private api: ApiService,
  ) {}

  /**
   * Получить список пользователей.
   */
  get$ = createEffect(() => this.actions$
    .pipe(
      ofType(UsersActionEnum.Get),
      exhaustMap(() => {
        let url = this.api.getUrl(ApiListTypeEnum.Users);
        return this.api.request<Array<UserModel>>(url, new RequestModel()).pipe(
          map(response => {
            return new EffectResponseModel(UsersActionEnum.GetOk, response);
          }),
          catchError((error) => {
            let message = `${UsersActionEnum.Get}: ${errorToString(error)}`;
            this.api.notification$.next(message);
            return of(new EffectResponseModel(UsersActionEnum.ApiError));
          })
        )
      })
    ), { dispatch: true }
  );

  /**
   * Добавить пользователя.
   */
  add$ = createEffect(() => this.actions$
    .pipe(
      ofType(UsersActionEnum.Add),
      exhaustMap((state: ActionPropsModel<UserModel>) => {
        let url = this.api.getUrl(ApiListTypeEnum.Users);
        return this.api.request<AuthResponseModel>(url, new RequestModel(RequestMethodEnum.Post, state.data)).pipe(
          map((response) => {
            this.api.notification$.next(ru_addedSuccessfully);
            return state.save
              ? new EffectResponseModel(UsersActionEnum.AddOk, response)
              : new EffectResponseModel(apiOk)
          }),
          catchError((error) => {
            let message = `${UsersActionEnum.Add}: ${errorToString(error)}`;
            this.api.notification$.next(message);
            return of(new EffectResponseModel(UsersActionEnum.ApiError));
          })
        )
      })
    ), { dispatch: true }
  );

  /**
   * Обновить пользователя.
   */
  put$ = createEffect(() => this.actions$
    .pipe(
      ofType(UsersActionEnum.Put),
      exhaustMap((state: ActionPropsModel<ItemModel<UserModel>>) => {
        let url = this.api.getUrl(ApiListTypeEnum.User, state.data?.id);
        return this.api.request<UserModel>(url, new RequestModel(RequestMethodEnum.Put, state.data?.item)).pipe(
          map(response => {
            this.api.notification$.next(ru_putedSuccessfully);
            return state.save
              ? new EffectResponseModel(UsersActionEnum.PutOk, response)
              : new EffectResponseModel(apiOk)
          }),
          catchError((error) => {
            let message = `${UsersActionEnum.Put}: ${errorToString(error)}`;
            this.api.notification$.next(message);
            return of(new EffectResponseModel(UsersActionEnum.ApiError));
          })
        )
      })
    ), { dispatch: true }
  );

  /**
   * Удалить пользователя.
   */
  del$ = createEffect(() => this.actions$
    .pipe(
      ofType(UsersActionEnum.Del),
      exhaustMap((state: ActionPropsModel<IdModel>) => {
        let id: number = state.data?.id ?? 0;
        let url = this.api.getUrl(ApiListTypeEnum.Users, id);
        return this.api.request(url, new RequestModel(RequestMethodEnum.Delete)).pipe(
          map(() => {
            this.api.notification$.next(ru_deletedSuccessfully);
            return state.save
              ? new EffectResponseModel(UsersActionEnum.DelOk, new IdModel(id))
              : new EffectResponseModel(apiOk)
          }),
          catchError((error) => {
            let message = `${UsersActionEnum.Del}: ${errorToString(error)}`;
            this.api.notification$.next(message);
            return of(new EffectResponseModel(UsersActionEnum.ApiError));
          })
        )
      })
    ), { dispatch: true }
  );

}

import { createReducer, on } from '@ngrx/store';

import { IUsersState } from './states';

import * as UsersAction from './actions';
import { isIdCorrect } from '../../services/helper.service';

export const initialState: IUsersState = {
  list: []
};

export const usersReducer = createReducer<IUsersState>(
  initialState,

  on(
    // Обработчик ошибки.
    UsersAction.apiError,
    (state) => {
      return { ...state, list: structuredClone(state.list ?? []) }
    }
  ),

  on(
    // Добавить список пользователей в state.
    UsersAction.getOk,
    (state, response) => {
      return { ...state, list: structuredClone(response?.data ?? []) }
    }
  ),

  on(
    // Добавить полльзователя в state.
    UsersAction.addOk,
    (state, response) => {
      let id = response?.data?.user?.id;

      let result = structuredClone(state.list ?? []);

      if (!isIdCorrect(id)) {
        // если пользователь пустой, то возвращаем прежний state.
        return { ...state, list: result }
      }

      result.push(response!.data!.user!);
      return { ...state, list: result }
    }
  ),

  on(
    // Обновить пользователя в state.
    UsersAction.putOk,
    (state, response) => {
      let id = response.data?.id;
      let result = structuredClone(state.list ?? []);

      if (!isIdCorrect(id)) {
        // если город пустой, то возвращаем прежний state.
        return { ...state, list: result }
      }

      let i = result.findIndex(x => (x?.id ?? 0) > 0 && x.id == response?.data?.id);

      if (i >= 0) {
        result[i] = structuredClone(response.data!);
      }

      return { ...state, list: result }
    }
  ),

  on(
    // Удалить пользователя из state.
    UsersAction.delOk,
    (state, response) => {
      let result = structuredClone(state.list?.filter(x => x.id != response?.data?.id) ?? []);
      return { ...state, list: result }
    }
  ),

  on(
    // Очистить пользователей.
    UsersAction.clear,
    (state) => {
      return { ...state, list: [] }
    }
  ),
);

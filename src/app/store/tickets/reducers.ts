import { createReducer, on } from '@ngrx/store';

import { ITicketsState } from './states';

import * as TicketsAction from './actions';
import { isIdCorrect } from '../../services/helper.service';

export const initialState: ITicketsState = {
  list: []
};

export const ticketsReducer = createReducer<ITicketsState>(
  initialState,

  on(
    // Обработчик ошибки.
    TicketsAction.apiError,
    (state) => {
      return { ...state, list: structuredClone(state.list ?? []) }
    }
  ),

  on(
    // Добавить один тикет в state.
    TicketsAction.getItemOk,
    (state, response) => {
      let id = response?.data?.id;
      let result = structuredClone(state.list ?? []);

      if (!isIdCorrect(id)) {
        // если тикет пустой, то возвращаем прежний state.
        return { ...state, list: result }
      }

      let i = result.findIndex(x => (x?.id ?? 0) > 0 && x.id == id);

      if (i >= 0) {
        // если тикет уже имеется в state, то обновляем state.
        result[i] = structuredClone(response!.data!);
        return { ...state, list: result }
      }

      // иначе добавить тикет в state.
      result.push(response.data!);
      return { ...state, list: result }
    }
  ),

  on(
    // Добавить список тикетов в state.
    TicketsAction.getListOk,
    (state, response) => {
      return { ...state, list: structuredClone(response?.data ?? []) }
    }
  ),

  on(
    // Добавить тикет в state.
    TicketsAction.addOk,
    (state, response) => {
      let id = response?.data?.id;
      let result = structuredClone(state.list ?? []);

      if (!isIdCorrect(id)) {
        // если тикет пустой, то возвращаем прежний state.
        return { ...state, list: result }
      }

      result.push(response!.data!);
      return { ...state, list: result }
    }
  ),

  on(
    // Обновить тикет в state.
    TicketsAction.putOk,
    (state, response) => {
      let id = response.data?.id;
      let result = structuredClone(state.list ?? []);

      if (!isIdCorrect(id)) {
        // если тикет пустой, то возвращаем прежний state.
        return { ...state, list: result }
      }

      let i = result.findIndex(x => (x?.id ?? 0) > 0 && x.id == response?.data?.id);

      if (i >= 0) {
        result[i] = structuredClone(response.data!);
      }

      return { ...state, list: result }
    }
  ),

  on(
    // Удалить тикет из state.
    TicketsAction.delOk,
    (state, response) => {
      let result = structuredClone(state.list?.filter(x => x.id != response?.data?.id) ?? []);
      return { ...state, list: result }
    }
  ),

  on(
    // Очистить тикеты.
    TicketsAction.clear,
    (state, response) => {
      return { ...state, list: [] }
    }
  ),
);

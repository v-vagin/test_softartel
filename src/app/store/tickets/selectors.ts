import { createFeatureSelector, createSelector } from "@ngrx/store";

import { ticketsFeatureName, ITicketsState } from "./states";

/**
 * Список тикетов.
 */
export const all = createSelector(
  createFeatureSelector<ITicketsState>(ticketsFeatureName),
  (state) => {
    return state.list ?? [];
  }
);

/**
 * Количество тикетов.
 */
export const counts = createSelector(
  all,
  (tickets) => {
    return tickets?.length ?? 0;
  }
);

/**
 * Данные тикета по ID.
 * @param id ID тикета.
 * @returns Данные тикета.
 */
export const byId = (id: number | null) => createSelector(
  all,
  (tickets) => {
    return tickets?.find(x => x.id === id) ?? null;
  }
);

/**
 * Первый тикет из списка.
 * @returns Данные тикета.
 */
export const first = createSelector(
  all,
  (tickets) => {
    return (tickets?.length ?? 0) >= 1
      ? tickets[0]
      : null;
  }
);

/**
 * Последний тикет из списка.
 * @returns Данные тикета.
 */
export const last = createSelector(
  all,
  (tickets) => {
    let i = tickets?.length ?? 0;
    return (i) >= 1
      ? tickets[i - 1]
      : null;
  }
);

/**
 * Доступные билеты.
 * @param id Список ID уже имеющихся тикетов.
 * @returns Список доступных тикетов.
 */
export const available = (ids: Array<number> | null) => createSelector(
  all,
  (tickets) => {
    return tickets?.filter(x => (x?.id ?? 0) > 0 && !ids?.includes(x!.id!)).map(x => x.id) ?? [];
  }
);

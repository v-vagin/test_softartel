import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, concatMap, exhaustMap, from, map, of } from 'rxjs';

import { ApiService } from '../../services/api.service';
import { errorToString } from '../../services/helper.service';

import { apiOk, ru_addedSuccessfully, ru_deletedSuccessfully, ru_putedSuccessfully } from '../../consts/global-const';

import { ApiListTypeEnum } from '../../enums/api-list-type-enum';
import { RequestMethodEnum } from '../../enums/request-method-enum';
import { TicketsActionEnum } from './actions';

import { TicketModel } from '../../models/ticket-model';
import { RequestModel } from '../../models/request-model';
import { ActionPropsModel } from '../../models/action-props-model';
import { IdListModel } from '../../models/id-list-model';
import { ItemModel } from '../../models/item-model';
import { EffectResponseModel } from '../../models/effect-response-model';
import { IdModel } from '../../models/id-model';

@Injectable()
export class TicketsEffects {
  constructor(
    private actions$: Actions,
    private api: ApiService,
  ) {}

  /**
   * Получить список тикетов.
   */
  get$ = createEffect(() => this.actions$
    .pipe(
      ofType(TicketsActionEnum.Get),
      exhaustMap((state: ActionPropsModel<IdListModel>) => {
        if (state.data?.all) {
          // весь список тикетов разом.
          let url = this.api.getUrl(ApiListTypeEnum.Tickets);
          return this.api.request<Array<TicketModel>>(url, new RequestModel()).pipe(
            map(response => {
              return new EffectResponseModel(TicketsActionEnum.GetListOk, response)
            }),
            catchError((error) => {
              let message = `${TicketsActionEnum.Get}: ${errorToString(error)}`;
              this.api.notification$.next(message);
              return of(new EffectResponseModel(TicketsActionEnum.ApiError));
            })
          );
        }

        // по одному тикету.
        return from(state.data?.list ?? []).pipe(
          concatMap(id => {
            let url = this.api.getUrl(ApiListTypeEnum.Ticket, id);
            return this.api.request<TicketModel>(url, new RequestModel()).pipe(
              map(response => {
                return new EffectResponseModel(TicketsActionEnum.GetItemOk, response)
              }),
              catchError((error) => {
                let message = `${TicketsActionEnum.Get}: ${errorToString(error)}`;
                this.api.notification$.next(message);
                return of(new EffectResponseModel(TicketsActionEnum.ApiError));
              })
            );
          })
        );
      })
    ), { dispatch: true }
  );

  /**
   * Добавить тикет.
   */
  add$ = createEffect(() => this.actions$
    .pipe(
      ofType(TicketsActionEnum.Add),
      exhaustMap((state: ActionPropsModel<TicketModel>) => {
        let url = this.api.getUrl(ApiListTypeEnum.Tickets);
        return this.api.request<TicketModel>(url, new RequestModel(RequestMethodEnum.Post, state.data)).pipe(
          map(response => {
            this.api.notification$.next(ru_addedSuccessfully);
            return state.save
              ? new EffectResponseModel(TicketsActionEnum.AddOk, response)
              : new EffectResponseModel(apiOk)
          }),
          catchError((error) => {
            let message = `${TicketsActionEnum.Add}: ${errorToString(error)}`;
            this.api.notification$.next(message);
            return of(new EffectResponseModel(TicketsActionEnum.ApiError));
          })
        );
      })
    ), { dispatch: true }
  );

  /**
   * Обновить тикет.
   */
  put$ = createEffect(() => this.actions$
    .pipe(
      ofType(TicketsActionEnum.Put),
      exhaustMap((state: ActionPropsModel<ItemModel<TicketModel>>) => {
        let url = this.api.getUrl(ApiListTypeEnum.Tickets, state.data?.id);
        return this.api.request<TicketModel>(url, new RequestModel(RequestMethodEnum.Put, state.data?.item)).pipe(
          map(response => {
            this.api.notification$.next(ru_putedSuccessfully);
            return state.save
              ? new EffectResponseModel(TicketsActionEnum.PutOk, response)
              : new EffectResponseModel(apiOk)
          }),
          catchError((error) => {
            let message = `${TicketsActionEnum.Put}: ${errorToString(error)}`;
            this.api.notification$.next(message);
            return of(new EffectResponseModel(TicketsActionEnum.ApiError));
          })
        );
      })
    ), { dispatch: true }
  );

  /**
   * Удалить тикет.
   */
  del$ = createEffect(() => this.actions$
    .pipe(
      ofType(TicketsActionEnum.Del),
      exhaustMap((state: ActionPropsModel<IdModel>) => {
        let id: number = state.data?.id ?? 0;
        let url = this.api.getUrl(ApiListTypeEnum.Ticket, id);
        return this.api.request(url, new RequestModel(RequestMethodEnum.Delete)).pipe(
          map(() => {
            this.api.notification$.next(ru_deletedSuccessfully);
            return state.save
              ? new EffectResponseModel(TicketsActionEnum.DelOk, new IdModel(id))
              : new EffectResponseModel(apiOk)
          }),
          catchError((error) => {
            let message = `${TicketsActionEnum.Del}: ${errorToString(error)}`;
            this.api.notification$.next(message);
            return of(new EffectResponseModel(TicketsActionEnum.ApiError));
          })
        );
      })
    ), { dispatch: true }
  );

}

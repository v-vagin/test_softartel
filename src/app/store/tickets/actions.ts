import { createAction, props } from "@ngrx/store";

import { IdModel } from "../../models/id-model";
import { TicketModel } from "../../models/ticket-model";
import { ActionPropsModel } from "../../models/action-props-model";
import { IdListModel } from "../../models/id-list-model";
import { ItemModel } from "../../models/item-model";

export enum TicketsActionEnum {
  // Actions для Store Эффектов.
  Add         = '[Tickets] Add', // Получить данные с БД API сервера.
  Get         = '[Tickets] Get', // Добавить данные в БД API сервера.
  Put         = '[Tickets] Put', // Обновить данные в БД API сервера.
  Del         = '[Tickets] Del', // Удалить данные из БД API сервера.

  // Actions для Store Редукторов.
  ApiError    = '[Tickets] ApiError', // Запрос в БД API сервера завершён с ошибкой, которую необходимо обработать.
  GetItemOk   = '[Tickets] GetItemOk', // Элемент списка получен с БД API сервера и его необходимо сохранить в Store.
  GetListOk   = '[Tickets] GetListOk', // Список получен с БД API сервера и его необходимо сохранить в Store.
  AddOk       = '[Tickets] AddOk', // Данные добавлены в БД API сервера и их необходимо добавить в Store.
  PutOk       = '[Tickets] PutOk', // Данные обновлены в БД API сервера и их необходимо обновить в Store.
  DelOk       = '[Tickets] DelOk', // Данные удалены из БД API сервера и их необходимо удалить из Store.
  Clear       = '[Tickets] Clear', // Очистить данные в Store.
}

export const apiError = createAction(
  TicketsActionEnum.ApiError
);

export const get = createAction(
  TicketsActionEnum.Get,
  props<ActionPropsModel<IdListModel>>()
);

export const getItemOk = createAction(
  TicketsActionEnum.GetItemOk,
  props<ActionPropsModel<TicketModel>>()
);

export const getListOk = createAction(
  TicketsActionEnum.GetListOk,
  props<ActionPropsModel<Array<TicketModel>>>()
);

export const add = createAction(
  TicketsActionEnum.Add,
  props<ActionPropsModel<TicketModel>>()
);

export const addOk = createAction(
  TicketsActionEnum.AddOk,
  props<ActionPropsModel<TicketModel>>()
);

export const put = createAction(
  TicketsActionEnum.Put,
  props<ActionPropsModel<ItemModel<TicketModel>>>()
);

export const putOk = createAction(
  TicketsActionEnum.PutOk,
  props<ActionPropsModel<TicketModel>>()
);

export const del = createAction(
  TicketsActionEnum.Del,
  props<ActionPropsModel<IdModel>>()
);

export const delOk = createAction(
  TicketsActionEnum.DelOk,
  props<ActionPropsModel<IdModel>>()
);

export const clear = createAction(
  TicketsActionEnum.Clear
);

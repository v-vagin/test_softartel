import { TicketModel } from "../../models/ticket-model";

export const ticketsFeatureName = 'tickets';

export interface ITicketsState {
  list: Array<TicketModel> | null;
}

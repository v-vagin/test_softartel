import { Params, RouterStateSnapshot } from "@angular/router";
import { RouterStateSerializer } from "@ngrx/router-store";

export const routerFeatureName = 'router';

export interface IRouterState {
  url: string;
  params: Params | null;
  queryParams: Params | null;
  fragment: string | null;
}

/**
 * Ограничиваем список данных роутера хранимых в Store.
 */
export class AppRouterSerializer implements RouterStateSerializer<IRouterState> {
  public serialize(routerState: RouterStateSnapshot): IRouterState {
    let route = routerState.root;

    while (route.firstChild) {
      route = route.firstChild;
    }

    let url = routerState.url;
    let queryParams = routerState.root.queryParams;
    let params = route.params;
    let fragment = routerState.root.fragment;

    return {url, params, queryParams, fragment };
  }

}

import { RouterReducerState } from "@ngrx/router-store";

import { IRouterState } from "./states";

export const initialState: RouterReducerState<IRouterState> = {
  state: { url: '', params: null, queryParams: null, fragment: null },
  navigationId: 0,
};

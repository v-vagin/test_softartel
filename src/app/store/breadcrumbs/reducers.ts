import { createReducer, on } from '@ngrx/store';

import { IBreadcrumbsState } from './states';

import * as BreadcrumbsAction from './actions';

export const initialState: IBreadcrumbsState = {
  list: []
};

export const breadcrumbsReducer = createReducer<IBreadcrumbsState>(
  initialState,

  on(
    // Обновить "хлебные крошки".
    BreadcrumbsAction.put,
    (state, data) => {
      if ((data?.list?.length ?? 0) == 0) {
        return { ...state, list: structuredClone(state.list) }
      }

      return { ...state, list: structuredClone(data.list) }
    }
  ),

  on(
    // Очистить "хлебные крошки".
    BreadcrumbsAction.clear,
    (state) => {
      return { ...state, list: [] }
    }
  ),
);

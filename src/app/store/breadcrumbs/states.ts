import { BreadcrumbModel } from "../../models/breadcrumb-model";

export const breadcrumbsFeatureName = 'breadcrumbs';

export interface IBreadcrumbsState {
  list: Array<BreadcrumbModel> | null;
}

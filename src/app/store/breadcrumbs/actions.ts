import { createAction, props } from "@ngrx/store";
import { BreadcrumbModel } from "../../models/breadcrumb-model";

export enum BreadcrumbsActionEnum {
  Put         = '[Breadcrumbs] Put',
  Clear       = '[Breadcrumbs] Clear',
}

export const put = createAction(
  BreadcrumbsActionEnum.Put,
  props<{ list: Array<BreadcrumbModel> | null }>()
);

export const clear = createAction(
  BreadcrumbsActionEnum.Clear
);

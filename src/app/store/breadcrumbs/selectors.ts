import { createFeatureSelector, createSelector } from "@ngrx/store";

import { breadcrumbsFeatureName, IBreadcrumbsState } from "./states";

/**
 * Список "хлебных крошек".
 */
export const all = createSelector(
  createFeatureSelector<IBreadcrumbsState>(breadcrumbsFeatureName),
  (state) => {
    return state.list;
  }
);

import { createReducer, on } from '@ngrx/store';

import { IPageInfoState } from './states';

import * as TitleAction from './actions';
import { PageInfoModel } from '../../models/page-info-model';

export const initialState: IPageInfoState = {
  item: null
};

export const pageInfoReducer = createReducer<IPageInfoState>(
  initialState,

  on(
    // Обновить информацию о странице.
    TitleAction.put,
    (state, data) => {
      if (!data?.item) {
        return { ...state, item: null }
      }

      return { ...state, item: structuredClone(data.item) }
    }
  ),

  on(
    // Очистить информацию о странице.
    TitleAction.clear,
    (state) => {
      return { ...state, item: null }
    }
  ),
);

import { PageInfoModel } from "../../models/page-info-model";

export const pageInfoFeatureName = 'pageInfo';

export interface IPageInfoState {
  item: PageInfoModel | null;
}

import { createFeatureSelector, createSelector } from "@ngrx/store";

import { pageInfoFeatureName, IPageInfoState } from "./states";

/**
 * Данные страницы.
 */
export const page = createSelector(
  createFeatureSelector<IPageInfoState>(pageInfoFeatureName),
  (state) => {
    return state.item;
  }
);

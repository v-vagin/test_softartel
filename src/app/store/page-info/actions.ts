import { createAction, props } from "@ngrx/store";
import { PageInfoModel } from "../../models/page-info-model";

export enum TitleActionEnum {
  Put         = '[PageInfo] Put',
  Clear       = '[PageInfo] Clear',
}

export const put = createAction(
  TitleActionEnum.Put,
  props<{ item: PageInfoModel }>()
);

export const clear = createAction(
  TitleActionEnum.Clear
);


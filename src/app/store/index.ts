import { isDevMode } from '@angular/core';
import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { routerReducer, RouterReducerState } from '@ngrx/router-store';
import { routerFeatureName, IRouterState } from './router/states';

import { citiesFeatureName, ICitiesState } from './cities/states';
import { citiesReducer } from './cities/reducers';

import { ticketsFeatureName, ITicketsState } from './tickets/states';
import { ticketsReducer } from './tickets/reducers';

import { usersFeatureName, IUsersState } from './users/states';
import { usersReducer } from './users/reducers';

import { userFeatureName, IUserState } from './user/states';
import { userReducer } from './user/reducers';

import { breadcrumbsFeatureName, IBreadcrumbsState } from './breadcrumbs/states';
import { breadcrumbsReducer } from './breadcrumbs/reducers';

import { pageInfoFeatureName, IPageInfoState } from './page-info/states';
import { pageInfoReducer } from './page-info/reducers';

export interface State {
  [routerFeatureName]: RouterReducerState<IRouterState>;
  [citiesFeatureName]: ICitiesState;
  [ticketsFeatureName]: ITicketsState;
  [usersFeatureName]: IUsersState;
  [userFeatureName]: IUserState;
  [breadcrumbsFeatureName]: IBreadcrumbsState;
  [pageInfoFeatureName]: IPageInfoState;
}

export const reducers: ActionReducerMap<State> = {
  [routerFeatureName]: routerReducer,
  [citiesFeatureName]: citiesReducer,
  [ticketsFeatureName]: ticketsReducer,
  [usersFeatureName]: usersReducer,
  [userFeatureName]: userReducer,
  [breadcrumbsFeatureName]: breadcrumbsReducer,
  [pageInfoFeatureName]: pageInfoReducer,
};

export const metaReducers: MetaReducer<State>[] = isDevMode() ? [] : [];

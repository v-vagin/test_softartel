import { createFeatureSelector, createSelector } from "@ngrx/store";

import { userFeatureName, IUserState } from "./states";

import * as Tickets from "../tickets/selectors";

/**
 * Данные авторизованного пользователя.
 */
export const user = createSelector(
  createFeatureSelector<IUserState>(userFeatureName),
  (state) => {
    return state.user;
  }
);

/**
 * true - если авторизованный пользователь является адимнистратором (ID = 1).
 */
export const isAdmin = createSelector(
  user,
  (user) => {
    let result = (user?.id ?? 0) == 1;
    return result;
  }
);

/**
 * Список ID тикетов авторизованного пользователя.
 */
const ticketsIds = createSelector(
  user,
  (user) => {
    return user?.tickets ?? [];
  }
);


/**
 * Количество тикетов авторизованного пользователя.
 * Если авторизованный пользователь admin (ID = 1),
 * то возвращается количество всех билетов.
 */
export const ticketsNumber = createSelector(
  user,
  Tickets.all,
  (user, tickets) => {
    if ((user?.id ?? 0) == 1) {
      return tickets?.length ?? 0;
    }

    return user?.tickets?.length ?? 0;
  }
);

/**
 * Список тикетов авторизованного пользователя.
 * Если авторизованный пользователь admin (ID = 1),
 * то возвращается количество всех билетов.
 * @returns Список тикетов.
 */
export const tickets = createSelector(
  user,
  ticketsIds,
  Tickets.all,
  (user, ticketsIds, tickets) => {
    if ((user?.id ?? 0) == 1) {
      return tickets ?? [];
    }

    return tickets?.filter(x => (x?.id ?? 0) > 0 && ticketsIds?.includes(x!.id!)) ?? [];
  }
);

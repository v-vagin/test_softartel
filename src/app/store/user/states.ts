import { UserModel } from "../../models/user-model";

export const userFeatureName = 'user';

export interface IUserState {
  user: UserModel | null;
}

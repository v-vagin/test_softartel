import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, exhaustMap, map, of } from 'rxjs';

import { ApiService } from '../../services/api.service';
import { errorToString } from '../../services/helper.service';

import { apiOk, ru_putedSuccessfully } from '../../consts/global-const';

import { ApiListTypeEnum } from '../../enums/api-list-type-enum';
import { RequestMethodEnum } from '../../enums/request-method-enum';
import { UserActionEnum } from './actions';

import { UserModel } from '../../models/user-model';
import { RequestModel } from '../../models/request-model';
import { ActionPropsModel } from '../../models/action-props-model';
import { ItemModel } from '../../models/item-model';
import { EffectResponseModel } from '../../models/effect-response-model';

@Injectable()
export class UserEffects {
  constructor(
    private actions$: Actions,
    private api: ApiService,
  ) {}

  /**
   * Обновить пользователя.
   */
  put$ = createEffect(() => this.actions$
    .pipe(
      ofType(UserActionEnum.Put),
      exhaustMap((state: ActionPropsModel<ItemModel<UserModel>>) => {
        let url = this.api.getUrl(ApiListTypeEnum.User, state.data?.id);
        return this.api.request<UserModel>(url, new RequestModel(RequestMethodEnum.Put, state.data?.item)).pipe(
          map(response => {
            this.api.notification$.next(ru_putedSuccessfully);
            return state.save
              ? new EffectResponseModel(UserActionEnum.PutOk, response)
              : new EffectResponseModel(apiOk)
          }),
          catchError((error) => {
            let message = `${UserActionEnum.Put}: ${errorToString(error)}`;
            this.api.notification$.next(message);
            return of(new EffectResponseModel(UserActionEnum.ApiError));
          })
        )
      })
    ), { dispatch: true }
  );

}

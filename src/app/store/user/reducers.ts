import { createReducer, on } from '@ngrx/store';

import { IUserState } from './states';

import * as UserAction from './actions';

export const initialState: IUserState = {
  user: null
};

export const userReducer = createReducer<IUserState>(
  initialState,

  on(
    // Обработчик ошибки.
    UserAction.apiError,
    (state) => {
      return { ...state, user: structuredClone(state.user) }
    }
  ),

  on(
    // Обновить авторизованного (локального) пользователя.
    UserAction.putOk,
    (state, response) => {
      if (!response.data) {
        return { ...state, user: null }
      }

      let result = structuredClone(response.data);
      result.password = null;
      return { ...state, user: result }
    }
  ),
);

import { createAction, props } from "@ngrx/store";

import { UserModel } from "../../models/user-model";
import { ActionPropsModel } from "../../models/action-props-model";
import { ItemModel } from "../../models/item-model";

export enum UserActionEnum {
  // Actions для Store Эффектов.
  Put         = '[User] Put', // Обновить данные в БД API сервера.

  // Actions для Store Редукторов.
  ApiError    = '[User] ApiError', // Запрос в БД API сервера завершён с ошибкой, которую необходимо обработать.
  PutOk       = '[User] PutOk', // Данные обновлены в БД API сервера и их необходимо обновить в Store.
}

export const apiError = createAction(
  UserActionEnum.ApiError
);

export const put = createAction(
  UserActionEnum.Put,
  props<ActionPropsModel<ItemModel<UserModel>>>()
);

export const putOk = createAction(
  UserActionEnum.PutOk,
  props<ActionPropsModel<UserModel>>()
);


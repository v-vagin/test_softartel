import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, Routes } from '@angular/router';

import { ApiService } from './services/api.service';
import { StorageService } from './services/storage.service';
import { PageDataService as TicketPageDataService } from './pages/ticket/page-data.service';
import { PageDataService as TicketsPageDataService } from './pages/tickets/page-data.service';
import { PageDataService as ProfilePageDataService } from './pages/profile/page-data.service';
import { PageDataService as DashboardCitiesPageDataService } from './pages/dashboard-cities/page-data.service';
import { PageDataService as DashboardTicketsPageDataService } from './pages/dashboard-tickets/page-data.service';
import { PageDataService as DashboardUsersPageDataService } from './pages/dashboard-users/page-data.service';

import { SegmentEnum } from './enums/segment-enum';

import { IndexComponent } from './pages/index/index.component';
import { LoginComponent } from './pages/login/login.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { TicketComponent } from './pages/ticket/ticket.component';
import { TicketsComponent } from './pages/tickets/tickets.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { DataNotFoundComponent } from './pages/data-not-found/data-not-found.component';
import { ServerNotAvailableComponent } from './pages/server-not-available/server-not-available.component';
import { DashboardUsersComponent } from './pages/dashboard-users/dashboard-users.component';
import { DashboardTicketsComponent } from './pages/dashboard-tickets/dashboard-tickets.component';
import { DashboardCitiesComponent } from './pages/dashboard-cities/dashboard-cities.component';

export const routes: Routes = [
  {
    path: '',
    runGuardsAndResolvers: 'always',
    canActivate: [ () => inject(ApiService).checkAccessRights() ],
    resolve: { _: () => inject(StorageService).initStore() },
    children: [
      { path: '', component: IndexComponent, pathMatch: 'full' },
      { path: SegmentEnum.Main, redirectTo: '' },
      {
        path: `${SegmentEnum.Profile}/:id`, component: ProfileComponent,
        resolve: { _: (route: ActivatedRouteSnapshot) =>
          inject(ProfilePageDataService).resolver(route)
        }
      },
      {
        path: `${SegmentEnum.Ticket}/:id`, component: TicketComponent,
        resolve: { _: (route: ActivatedRouteSnapshot) =>
          inject(TicketPageDataService).resolver(route)
        }
      },
      {
        path: SegmentEnum.Tickets, component: TicketsComponent,
        resolve: { _: () =>
          inject(TicketsPageDataService).resolver()
        }
      },
      {
        path: `${SegmentEnum.Dashboard}`,
        children: [
          { path: '', component: DashboardComponent, pathMatch: 'full' },
          { path: SegmentEnum.Dashboard, redirectTo: '' },
          {
            path: 'users', component: DashboardUsersComponent,
            resolve: { _: () =>
              inject(DashboardUsersPageDataService).resolver()
            }
          },
          {
            path: 'tickets', component: DashboardTicketsComponent,
            resolve: { _: () =>
              inject(DashboardTicketsPageDataService).resolver()
            }
          },
          {
            path: 'cities', component: DashboardCitiesComponent,
            resolve: { _: () =>
              inject(DashboardCitiesPageDataService).resolver()
            }
          },
        ]
      },
    ]
  },
  { path: SegmentEnum.Login, component: LoginComponent },
  { path: SegmentEnum.DataNotFound, component: DataNotFoundComponent },
  { path: SegmentEnum.ServerNotAvailable, component: ServerNotAvailableComponent },
  { path: '**', component: PageNotFoundComponent },
];

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router } from '@angular/router';

import { firstValueFrom, take } from 'rxjs';
import { Store } from '@ngrx/store';

import { ApiService } from '../../services/api.service';
import { StorageService } from '../../services/storage.service';
import { isIdCorrect, parseError } from '../../services/helper.service';

import { incorrectId, storeDataNotFound } from '../../consts/global-const';

import { SegmentEnum } from '../../enums/segment-enum';
import { SegmentNameEnum } from '../../enums/segment-name-enum';

import { BreadcrumbModel } from '../../models/breadcrumb-model';
import { ActionPropsModel } from '../../models/action-props-model';

import * as UserSelector from '../../store/user/selectors';
import * as TicketsAction from '../../store/tickets/actions';

import { TicketModel } from '../../models/ticket-model';
import { RequestModel } from '../../models/request-model';
import { ApiListTypeEnum } from '../../enums/api-list-type-enum';

@Injectable({
  providedIn: 'root'
})
export class PageDataService {

  constructor(
    private router: Router,
    private store: Store,
    private api: ApiService,
    private storage: StorageService,
  ) { }

  /**
   * Загрузчик данных для страницы.
   */
  public async resolver(route: ActivatedRouteSnapshot) {
    // Получаем ID тикета из URL строки.
    let paramId = route?.params['id'];

    try {
      if (!isIdCorrect(paramId)) {
        throw new Error(incorrectId, { cause: 404 });
      }

      // Получаем информацию авторизованного пользователя.
      let user = await firstValueFrom(this.store.select(UserSelector.user).pipe(take(1)));
      let userId = user?.id;

      if (!isIdCorrect(userId)) {
        throw new Error(storeDataNotFound, { cause: 404 });
      }

      let id: number = paramId;

      if (userId! > 1) {
        // Пользователь не Админ, поэтому првоеряем ID тикета по списку тикетов авторизованного пользователя.
        let ticketId = user!.tickets?.find(x => x == paramId);

        if (!isIdCorrect(ticketId)) {
          throw new Error(incorrectId, { cause: 404 });
        }

        id = ticketId!;
      }

      let url = this.api.getUrl(ApiListTypeEnum.Ticket, id);
      let response = await firstValueFrom(this.api.request<TicketModel>(url, new RequestModel()));

      if (!isIdCorrect(response.id)) {
        throw new Error(incorrectId, { cause: 404 });
      }

      // Помещаем полученный тикет в Store.
      this.store.dispatch(TicketsAction.getItemOk(new ActionPropsModel<TicketModel>(response)));

    } catch (error) {
      let err = parseError(error);
      switch (err.status) {
        case 404:
          console.warn(err.statusText);
          this.router.navigateByUrl(`/${SegmentEnum.DataNotFound}`);
          return;

        default:
          console.warn(err.statusText);
          this.router.navigateByUrl(`/${SegmentEnum.ServerNotAvailable}`);
          return;
      }
    }

    this.storage.setPageTitle(SegmentNameEnum.Ticket, SegmentEnum.Ticket);
    this.storage.setPageBreadcrumbs(
      new Array<BreadcrumbModel>(
        new BreadcrumbModel(SegmentNameEnum.Main, '/'),
        new BreadcrumbModel(SegmentNameEnum.Tickets, `/${SegmentEnum.Tickets}`),
        new BreadcrumbModel(`${SegmentNameEnum.Ticket} - #${paramId}`),
      )
    );
  }

}

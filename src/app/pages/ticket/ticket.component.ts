import { Component, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule }  from '@angular/material/card';

import { Subject, takeUntil } from 'rxjs';
import { Store } from '@ngrx/store';

import { TicketModel } from '../../models/ticket-model';

import * as TicketsSelector from "../../store/tickets/selectors";

@Component({
  selector: 'app-ticket',
  standalone: true,
  imports: [
    CommonModule,
    MatCardModule,
  ],
  templateUrl: './ticket.component.html',
  styleUrl: './ticket.component.scss'
})
export class TicketComponent implements OnDestroy {
  private unsubsribe$: Subject<boolean> = new Subject();
  public ticket: TicketModel | null = null;

  constructor(
    private store: Store,
  ) {
    this.store.select(TicketsSelector.first)
      .pipe(takeUntil(this.unsubsribe$))
      .subscribe(ticket => this.ticket = structuredClone(ticket));
  }

  ngOnDestroy(): void {
    this.unsubsribe$.next(true);
    this.unsubsribe$.complete();
  }

}

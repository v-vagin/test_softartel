import { Component } from '@angular/core';

import { StorageService } from '../../services/storage.service';

import { SegmentEnum } from '../../enums/segment-enum';
import { SegmentNameEnum } from '../../enums/segment-name-enum';

@Component({
  selector: 'app-server-not-available',
  standalone: true,
  imports: [
  ],
  templateUrl: './server-not-available.component.html',
  styleUrl: './server-not-available.component.scss'
})
export class ServerNotAvailableComponent {

  constructor(
    private storage: StorageService,
  ) {
    this.storage.setPageTitle(SegmentNameEnum.ServerNotAvailable, SegmentEnum.ServerNotAvailable);
  }

}

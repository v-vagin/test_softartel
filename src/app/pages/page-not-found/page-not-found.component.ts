import { Component } from '@angular/core';

import { StorageService } from '../../services/storage.service';

import { SegmentEnum } from '../../enums/segment-enum';
import { SegmentNameEnum } from '../../enums/segment-name-enum';

@Component({
  selector: 'app-page-not-found',
  standalone: true,
  imports: [
  ],
  templateUrl: './page-not-found.component.html',
  styleUrl: './page-not-found.component.scss'
})
export class PageNotFoundComponent {

  constructor(
    private storage: StorageService,
  ) {
    this.storage.setPageTitle(SegmentNameEnum.PageNotFound, SegmentEnum.PageNotFound);
  }

}

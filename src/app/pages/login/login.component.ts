import { Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, Validators, FormsModule, ReactiveFormsModule, FormGroup } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

import { ApiService } from '../../services/api.service';
import { StorageService } from '../../services/storage.service';

import { LoginModel } from '../../models/login-model';
import { SegmentEnum } from '../../enums/segment-enum';
import { SegmentNameEnum } from '../../enums/segment-name-enum';
import { ru_incorrectData, ru_serverNotAvailable } from '../../consts/global-const';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    ReactiveFormsModule,
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent implements OnDestroy {
  public formData = new FormGroup({
    email: new FormControl('admin@mail.ru', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(4)]),
  });
  /**
   * Показывать (true) текст пароля в форме или нет (false)
   */
  public isVisible: boolean = false;

  @ViewChild('passwordInput', { static: true }) passwordInput!: ElementRef;

  constructor(
    private router: Router,
    private apiService: ApiService,
    private storage: StorageService,
  ) {
    this.storage.setPageTitle(SegmentNameEnum.Login, SegmentEnum.Login);
    this.storage.setPageBreadcrumbs([]);
  }

  ngOnDestroy(): void {
  }

  /**
   * Авторизовать пользователя по логину и паролю.
   */
  public async doLogin(): Promise<void> {
    let email = this.formData.value.email?.trim()?.toLowerCase() ?? '';
    let password = this.formData.value.password?.trim() ?? '';

    if (!email || !password) {
      this.apiService.notification$.next(ru_incorrectData)
      return;
    }

    let id = await this.apiService.login(new LoginModel(email, password));

    if (!id) {
      this.apiService.notification$.next(ru_incorrectData)
      return;
    }

    if (id < 0) {
      this.apiService.notification$.next(ru_serverNotAvailable)
      return;
    }

    this.router.navigate(['/'], { replaceUrl: true });
  }

}

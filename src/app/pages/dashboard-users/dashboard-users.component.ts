import { Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { provideNativeDateAdapter } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';

import { Subject, Subscription, takeUntil } from 'rxjs';
import { Store } from '@ngrx/store';

import { UserModel } from '../../models/user-model';
import { CityModel } from '../../models/city-model';
import { ActionPropsModel } from '../../models/action-props-model';
import { ItemModel } from '../../models/item-model';

import * as UsersSelector from '../../store/users/selectors';
import * as UsersAction from '../../store/users/actions';
import * as TicketsSelector from '../../store/tickets/selectors';
import * as CitiesSelector from '../../store/cities/selectors';
import { isIdCorrect } from '../../services/helper.service';
import { IdModel } from '../../models/id-model';

@Component({
  selector: 'app-dashboard-users',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatDatepickerModule,
    MatSelectModule,
  ],
  providers: [
    provideNativeDateAdapter()
  ],
  templateUrl: './dashboard-users.component.html',
  styleUrl: './dashboard-users.component.scss'
})
export class DashboardUsersComponent implements OnDestroy {
  private unsubscribe$: Subject<boolean> = new Subject();
  private usersSubscription$: Subscription | null = null;
  public users$ = this.store.select(UsersSelector.all);
  public ticketById$ = (id: number | null) => this.store.select(TicketsSelector.byId(id));
  public availableTickets$ = (ids: Array<number> | null) => this.store.select(TicketsSelector.available(ids));

  public selectedAvailTicketId: number | null = null;
  public selectedUserTicketId: number | null = null;
  public user: UserModel | null = null;
  /**
   * Показывать (true) текст пароля в форме или нет (false)
   */
  public isVisible: boolean = false;
  /**
   * Режим работы компонента:
   * false (по умолчанию) - режим просмотра,
   * true - режим редактирвоания профиля.
   */
  public isEditMode: boolean = false;
  public cities: CityModel[] = [];
  public userDataForm = new FormGroup({
    email: new FormControl({ value: this.user?.email, disabled: true }, [Validators.required, Validators.email]),
    password: new FormControl(this.user?.password, [Validators.required, Validators.minLength(4)]),
    name: new FormControl({ value: this.user?.name, disabled: true }, [Validators.required, Validators.minLength(1)]),
    lastname: new FormControl({ value: this.user?.lastname, disabled: true }),
    bDate: new FormControl<Date | null>({ value: null, disabled: true })
  });
  /**
   * true - если ожидается получение добавленного элемента.
   */
  private isWaitResponse: boolean = false;

  @ViewChild('passwordInput', { static: false }) passwordInput!: ElementRef;

  constructor(
    private store: Store,
  ) {
    this.store.select(CitiesSelector.all)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(cities => this.cities = structuredClone(cities));

    // Ожидается только что добавленный пользователь.
    this.store.select(UsersSelector.last)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(user => {
        if (this.isWaitResponse) {
          this.updateUserDataForm(user);
          this.isWaitResponse = false;
        }
      });
  }

  ngOnDestroy(): void {
    this.usersSubscription$?.unsubscribe();
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }

  /**
   * Изменить режим работы.
   * @param value true - режим редактирования, false - режим просмотра.
   */
  public changeMode(value: boolean): void {
    this.isEditMode = value;
    this.selectedUserTicketId = null;
    let email = this.userDataForm.get('email');
    let name = this.userDataForm.get('name');
    let lastname = this.userDataForm.get('lastname');

    if (value) {
      if ((this.user?.id ?? 0) == 0) {
        email?.enable({ onlySelf: true });
      }

      name?.enable({ onlySelf: true });
      lastname?.enable({ onlySelf: true });
      return;
    }

    if ((this.user?.id ?? 0) == 0) {
      email?.disable({ onlySelf: true });
    }

    name?.disable({ onlySelf: true });
    lastname?.disable({ onlySelf: true });
    this.selectedAvailTicketId = null;
    this.selectedUserTicketId = null;
  }

  /**
   * Обновить данные формы.
   * @param userData Данные для формы.
   */
  private updateUserDataForm(userData: UserModel | null): void {
    this.user = structuredClone(userData);

    if (this.user) {
      this.user.password = '';
    }

    this.userDataForm.patchValue({
      email: this.user?.email,
      name: this.user?.name,
      lastname: this.user?.lastname,
      bDate: !this.user?.bDate ? null : new Date(this.user.bDate!)
    });
  }

  /**
   * Сохранить изменения.
   */
  public saveData(): void {
    this.user!.email = !this.userDataForm.value.email ? this.user!.email : this.userDataForm.value.email;
    this.user!.name = this.userDataForm.value.name;
    this.user!.lastname = this.userDataForm.value.lastname;
    this.user!.password = this.userDataForm.value.password!;
    let id = this.user!.id!;

    if (isIdCorrect(id) && id! > 0) {
      let props = new ActionPropsModel(new ItemModel(id, structuredClone(this.user)));
      this.store.dispatch(UsersAction.put(props));
      this.changeMode(false);
      return;
    }

    this.isWaitResponse = true;
    let props = new ActionPropsModel(structuredClone(this.user));
    this.store.dispatch(UsersAction.add(props));
    this.changeMode(false);
  }

  /**
   * Выбрать элемент для редактирования.
   * @param item Данные пользователя.
   */
  public selectUser(id: number | null): void {
    if (this.isEditMode) {
      return;
    }

    this.usersSubscription$?.unsubscribe();
    this.usersSubscription$ = this.store.select(UsersSelector.byId(id))
      .subscribe(user => this.updateUserDataForm(user));
  }

  /**
   * Добавить нового пользователя.
   */
  public addUser(): void {
    this.updateUserDataForm(new UserModel(''));
    this.changeMode(true);
  }

  public delUser(): void {
    let props = new ActionPropsModel(new IdModel(this.user!.id!));
    this.store.dispatch(UsersAction.del(props));
    this.updateUserDataForm(null);
  }

  /**
   * Отметить тикет для последующего добавления тикета пользователю.
   * @param id ID тикета.
   */
  public selectAvailTicket(id: number | null): void {
    if (!this.isEditMode) {
      return;
    }

    this.selectedAvailTicketId = id;
  }

  /**
   * Отметить тикет для последующего удаления тикета у пользователю.
   * @param id ID тикета.
   */
  public selectUserTicket(id: number | null): void {
    if (!this.isEditMode) {
      return;
    }

    this.selectedUserTicketId = id;
  }

  /**
   * Добавить тикет пользователю.
   */
  public addTicketToUser(): void {
    let tickets = structuredClone(this.user?.tickets ?? []);
    tickets.push(this.selectedAvailTicketId!);
    this.user!.tickets = tickets;
    this.selectedAvailTicketId = null;
  }

  /**
   * Удалить тикет у пользователя.
   */
  public deleteTicketFromUser(): void {
    this.user!.tickets = this.user!.tickets!.filter(id => id != this.selectedUserTicketId);
    this.selectedUserTicketId = null;
  }


}

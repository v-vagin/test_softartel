import { Injectable } from '@angular/core';

import { Store } from '@ngrx/store';

import { StorageService } from '../../services/storage.service';

import { SegmentEnum } from '../../enums/segment-enum';
import { SegmentNameEnum } from '../../enums/segment-name-enum';

import { BreadcrumbModel } from '../../models/breadcrumb-model';
import { ActionPropsModel } from '../../models/action-props-model';
import { IdListModel } from '../../models/id-list-model';

import * as CitiesAction from '../../store/cities/actions';
import * as UsersAction from '../../store/users/actions';
import * as TicketsAction from '../../store/tickets/actions';

@Injectable({
  providedIn: 'root'
})
export class PageDataService {

  constructor(
    private store: Store,
    private storage: StorageService,
  ) { }

  /**
   * Загрузчик данных для страницы.
   */
  public resolver() {
    this.store.dispatch(CitiesAction.get());
    this.store.dispatch(UsersAction.get());
    this.store.dispatch(TicketsAction.get(new ActionPropsModel<IdListModel>({ all: true })));

    this.storage.setPageTitle(SegmentNameEnum.Cities, SegmentEnum.Cities);
    this.storage.setPageBreadcrumbs(
      new Array<BreadcrumbModel>(
        new BreadcrumbModel(SegmentNameEnum.Main, '/'),
        new BreadcrumbModel(SegmentNameEnum.Dashboard, `/${SegmentEnum.Dashboard}`),
        new BreadcrumbModel(SegmentNameEnum.Users),
      )
    );
  }

}

import { Component, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';

import { Subject, takeUntil } from 'rxjs';
import { Store } from '@ngrx/store';
import { isIdCorrect } from '../../services/helper.service';

import { TicketModel } from '../../models/ticket-model';
import { ActionPropsModel } from '../../models/action-props-model';
import { IdModel } from '../../models/id-model';
import { ItemModel } from '../../models/item-model';

import * as TicketsSelector from '../../store/tickets/selectors';
import * as TicketsAction from '../../store/tickets/actions';

@Component({
  selector: 'app-dashboard-tickets',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
  ],
  templateUrl: './dashboard-tickets.component.html',
  styleUrl: './dashboard-tickets.component.scss'
})
export class DashboardTicketsComponent implements OnDestroy {
  private unsubscribe$: Subject<boolean> = new Subject();
  public tickets$ = this.store.select(TicketsSelector.all);
  public selectedItem: TicketModel | null = null;
  /**
   * Режим работы компонента:
   * false (по умолчанию) - режим просмотра,
   * true - режим редактирвоания профиля.
   */
  public isEditMode: boolean = false;
  /**
   * true - если ожидается получение добавленного элемента.
   */
  private isWaitResponse: boolean = false;

  public dataForm = new FormGroup({
    name: new FormControl({ value: this.selectedItem?.name, disabled: true }, [ Validators.required, Validators.minLength(3) ]),
  });

  constructor(
    private store: Store,
  ) {
    // Ожидается только что добавленный элемент.
    this.store.select(TicketsSelector.last)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(ticket => {
        if (this.isWaitResponse) {
          this.selectItem(ticket);
          this.isWaitResponse = false;
        }
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }

  /**
   * Изменить режим работы.
   * @param value true - режим редактирования, false - режим просмотра.
   */
  public changeMode(value: boolean): void {
    this.isEditMode = value;
    let name = this.dataForm.get('name');

    if (value) {
      name?.enable({ onlySelf: true });
      return;
    }

    name?.disable({ onlySelf: true });
  }

  /**
   * Создать новый элемент для редактирования.
   */
  public addItem(): void {
    this.changeMode(true);
    this.selectItem(new TicketModel(0, ''));
  }

  /**
   * Выбрать элемент для редактирования.
   * @param item Данные тикета.
   */
  public selectItem(item: TicketModel | null): void {
    this.selectedItem = structuredClone(item);
    this.dataForm.patchValue({
      name: this.selectedItem?.name,
    });
  }

  /**
   * Удалить выбранный элемент.
   */
  public deleteItem(): void {
    let props = new ActionPropsModel(new IdModel(this.selectedItem!.id!));
    this.store.dispatch(TicketsAction.del(props));
    this.selectItem(null);
  }

  /**
   * Сохранить изменения выбранного (редактируемого) элемента.
   */
  public saveItem() {
    this.changeMode(false);
    let id = this.selectedItem?.id;
    this.selectedItem!.name = this.dataForm.value.name ?? '';

    if (isIdCorrect(id) && id! > 0) {
      let props = new ActionPropsModel(new ItemModel(id, this.selectedItem));
      this.store.dispatch(TicketsAction.put(props));
      return;
    }

    this.isWaitResponse = true;
    let props = new ActionPropsModel(this.selectedItem);
    this.store.dispatch(TicketsAction.add(props));
  }
}

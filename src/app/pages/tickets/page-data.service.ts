import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { firstValueFrom, take } from 'rxjs';
import { Store } from '@ngrx/store';

import { StorageService } from '../../services/storage.service';
import { isIdCorrect } from '../../services/helper.service';

import { storeDataNotFound } from '../../consts/global-const';

import { SegmentEnum } from '../../enums/segment-enum';
import { SegmentNameEnum } from '../../enums/segment-name-enum';

import { BreadcrumbModel } from '../../models/breadcrumb-model';
import { ActionPropsModel } from '../../models/action-props-model';
import { IdListModel } from '../../models/id-list-model';

import * as UserSelector from '../../store/user/selectors';
import * as TicketsAction from '../../store/tickets/actions';

@Injectable({
  providedIn: 'root'
})
export class PageDataService {

  constructor(
    private router: Router,
    private store: Store,
    private storage: StorageService,
  ) { }

  /**
   * Загрузчик данных для страницы.
   */
  public async resolver() {
    // Получаем информацию авторизованного пользователя.
    let user = await firstValueFrom(this.store.select(UserSelector.user).pipe(take(1)));

    if (!isIdCorrect(user?.id)) {
      console.warn(storeDataNotFound);
      this.router.navigateByUrl(`/${SegmentEnum.DataNotFound}`);
      return;
    }

    if (user!.id == 1) {
      // Если пользователь Админ, то получаем список всех тикетов с API сервера.
      this.store.dispatch(TicketsAction.get(new ActionPropsModel<IdListModel>({ all: true })));
    }
    else {
      // Иначе получаем тикеты авторизованного пользователя, если они у него имеются.
      if ((user?.tickets?.length ?? 0) > 0) {
        this.store.dispatch(TicketsAction.get(new ActionPropsModel<IdListModel>({ list: user!.tickets })));
      }
    }

    this.storage.setPageTitle(SegmentNameEnum.Tickets, SegmentEnum.Tickets);
    this.storage.setPageBreadcrumbs(
      new Array<BreadcrumbModel>(
        new BreadcrumbModel(SegmentNameEnum.Main, '/'),
        new BreadcrumbModel(SegmentNameEnum.Tickets),
      )
    );
  }

}

import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';

import { Store } from '@ngrx/store';

import { isIdCorrect } from '../../services/helper.service';

import { SegmentEnum } from '../../enums/segment-enum';

import * as TicketsSelector from "../../store/tickets/selectors";

@Component({
  selector: 'app-tickets',
  standalone: true,
  imports: [
    CommonModule,
  ],
  templateUrl: './tickets.component.html',
  styleUrl: './tickets.component.scss'
})
export class TicketsComponent {
  public tickets$ = this.store.select(TicketsSelector.all);

  constructor(
    private router: Router,
    private store: Store,
  ) {
  }

  public goToTicket(id: number | null): void {
    if (!isIdCorrect) {
      return;
    }

    this.router.navigateByUrl(`/${SegmentEnum.Ticket}/${id}`);
  }
}

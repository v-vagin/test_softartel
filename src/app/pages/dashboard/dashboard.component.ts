import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { StorageService } from '../../services/storage.service';

import { SegmentEnum } from '../../enums/segment-enum';
import { SegmentNameEnum } from '../../enums/segment-name-enum';

import { BreadcrumbModel } from '../../models/breadcrumb-model';

@Component({
  selector: 'app-dashboard',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
  ],
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.scss'
})
export class DashboardComponent {

  constructor(
    private storage: StorageService,
  ) {
    this.storage.setPageTitle(SegmentNameEnum.Dashboard, SegmentEnum.Dashboard);
    this.storage.setPageBreadcrumbs(
      new Array<BreadcrumbModel>(
        new BreadcrumbModel(SegmentNameEnum.Main, '/'),
        new BreadcrumbModel(SegmentNameEnum.Dashboard),
      )
    );
  }

}

import { Component } from '@angular/core';

import { StorageService } from '../../services/storage.service';

import { SegmentEnum } from '../../enums/segment-enum';
import { SegmentNameEnum } from '../../enums/segment-name-enum';

@Component({
  selector: 'app-data-not-found',
  standalone: true,
  imports: [
  ],
  templateUrl: './data-not-found.component.html',
  styleUrl: './data-not-found.component.scss'
})
export class DataNotFoundComponent {

  constructor(
    private storage: StorageService,
  ) {
    this.storage.setPageTitle(SegmentNameEnum.DataNotFound, SegmentEnum.DataNotFound);
  }

}

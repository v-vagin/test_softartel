import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { Store } from '@ngrx/store';
import { StorageService } from '../../services/storage.service';

import { SegmentEnum } from '../../enums/segment-enum';
import { SegmentNameEnum } from '../../enums/segment-name-enum';

import { BreadcrumbModel } from '../../models/breadcrumb-model';

import * as UserSelector from '../../store/user/selectors';

@Component({
  selector: 'app-index',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
  ],
  templateUrl: './index.component.html',
  styleUrl: './index.component.scss'
})
export class IndexComponent {
  public readonly segmentEnum = SegmentEnum;
  public readonly segmentNameEnum = SegmentNameEnum;
  public isAdmin$ = this.store.select(UserSelector.isAdmin);

  constructor(
    private store: Store,
    private storage: StorageService,
  ) {
    this.storage.setPageTitle(SegmentNameEnum.Main, SegmentEnum.Main);
    this.storage.setPageBreadcrumbs(
      new Array<BreadcrumbModel>(
        new BreadcrumbModel(SegmentNameEnum.Main)
      )
    );
  }

}

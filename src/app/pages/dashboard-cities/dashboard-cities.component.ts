import { Component, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';

import { Subject, takeUntil } from 'rxjs';
import { Store } from '@ngrx/store';
import { StorageService } from '../../services/storage.service';
import { isIdCorrect } from '../../services/helper.service';

import { CityModel } from '../../models/city-model';
import { ActionPropsModel } from '../../models/action-props-model';
import { ItemModel } from '../../models/item-model';
import { IdModel } from '../../models/id-model';

import * as CitiesSelector from '../../store/cities/selectors';
import * as CitiesAction from '../../store/cities/actions';

@Component({
  selector: 'app-dashboard-cities',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
  ],
  templateUrl: './dashboard-cities.component.html',
  styleUrl: './dashboard-cities.component.scss'
})
export class DashboardCitiesComponent implements OnDestroy {
  private unsubscribe$: Subject<boolean> = new Subject();
  public clities$ = this.store.select(CitiesSelector.all);
  public selectedItem: CityModel | null = null;
  /**
   * Режим работы компонента:
   * false (по умолчанию) - режим просмотра,
   * true - режим редактирвоания профиля.
   */
  public isEditMode: boolean = false;
  /**
   * true - если ожидается получение добавленного элемента.
   */
  private isWaitResponse: boolean = false;

  public dataForm = new FormGroup({
    name: new FormControl({ value: this.selectedItem?.name, disabled: true }, [ Validators.required, Validators.minLength(3) ]),
  });

  constructor(
    private store: Store,
    private storage: StorageService,
  ) {
    // Ожидается только что добавленный элемент.
    this.store.select(CitiesSelector.last)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(city => {
        if (this.isWaitResponse) {
          this.selectItem(city);
          this.isWaitResponse = false;
        }
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }

  /**
   * Изменить режим работы.
   * @param value true - режим редактирования, false - режим просмотра.
   */
  public changeMode(value: boolean): void {
    this.isEditMode = value;
    let name = this.dataForm.get('name');

    if (value) {
      name?.enable({ onlySelf: true });
      return;
    }

    name?.disable({ onlySelf: true });
  }

  /**
   * Создать новый элемент для редактирования.
   */
  public addItem(): void {
    this.changeMode(true);
    this.selectItem(new CityModel(0, ''));
  }

  /**
   * Выбрать элемент для редактирования.
   * @param item Данные города.
   */
  public selectItem(item: CityModel | null): void {
    this.selectedItem = structuredClone(item);
    this.dataForm.patchValue({
      name: this.selectedItem?.name,
    });
  }

  /**
   * Удалить выбранный элемент.
   */
  public deleteItem(): void {
    let props = new ActionPropsModel(new IdModel(this.selectedItem!.id!));
    this.store.dispatch(CitiesAction.del(props));
    this.selectItem(null);
  }

  /**
   * Сохранить изменения выбранного (редактируемого) элемента.
   */
  public saveItem() {
    this.changeMode(false);
    let id = this.selectedItem?.id;
    this.selectedItem!.name = this.dataForm.value.name ?? '';

    if (isIdCorrect(id) && id! > 0) {
      let props = new ActionPropsModel(new ItemModel(id, this.selectedItem));
      this.store.dispatch(CitiesAction.put(props));
      return;
    }

    this.isWaitResponse = true;
    let props = new ActionPropsModel(this.selectedItem);
    this.store.dispatch(CitiesAction.add(props));
  }

}

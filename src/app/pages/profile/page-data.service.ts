import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router } from '@angular/router';

import { firstValueFrom, take } from 'rxjs';
import { Store } from '@ngrx/store';

import { StorageService } from '../../services/storage.service';
import { isIdCorrect } from '../../services/helper.service';

import { incorrectId, storeDataNotFound } from '../../consts/global-const';

import { SegmentEnum } from '../../enums/segment-enum';
import { SegmentNameEnum } from '../../enums/segment-name-enum';

import { BreadcrumbModel } from '../../models/breadcrumb-model';

import * as UserSelector from '../../store/user/selectors';
import * as CitiesAction from '../../store/cities/actions';

@Injectable({
  providedIn: 'root'
})
export class PageDataService {

  constructor(
    private router: Router,
    private store: Store,
    private storage: StorageService,
  ) { }

  /**
   * Загрузчик данных для страницы.
   */
  public async resolver(route: ActivatedRouteSnapshot) {
    // Получаем ID авторизованного пользователя из URL строки.
    let paramId = route?.params['id'];

    if (!isIdCorrect(paramId)) {
      console.warn(incorrectId)
      this.router.navigateByUrl(`/${SegmentEnum.DataNotFound}`);
      return;
    }

    // Получаем информацию авторизованного пользователя.
    let user = await firstValueFrom(this.store.select(UserSelector.user).pipe(take(1)));

    if (!isIdCorrect(user?.id) || paramId != user!.id) {
      console.warn(storeDataNotFound, 'or', incorrectId)
      this.router.navigateByUrl(`/${SegmentEnum.DataNotFound}`);
      return;
    }

    this.store.dispatch(CitiesAction.get());

    this.storage.setPageTitle(SegmentNameEnum.Profile, SegmentEnum.Profile);
    this.storage.setPageBreadcrumbs(
      new Array<BreadcrumbModel>(
        new BreadcrumbModel(SegmentNameEnum.Main, '/'),
        new BreadcrumbModel(`${SegmentNameEnum.Profile} - ${user?.name ?? 'Аноним'}`),
      )
    );
  }

}

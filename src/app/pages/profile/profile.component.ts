import { Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { provideNativeDateAdapter } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';

import { Subject, takeUntil } from 'rxjs';
import { Store } from '@ngrx/store';

import { UserModel } from '../../models/user-model';
import { CityModel } from '../../models/city-model';
import { ActionPropsModel } from '../../models/action-props-model';
import { ItemModel } from '../../models/item-model';

import * as UserAction from '../../store/user/actions';
import * as UserSelector from '../../store/user/selectors';
import * as CitiesSelector from '../../store/cities/selectors';

@Component({
  selector: 'app-profile',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatDatepickerModule,
    MatSelectModule,
  ],
  providers: [
    provideNativeDateAdapter()
  ],
  templateUrl: './profile.component.html',
  styleUrl: './profile.component.scss'
})
export class ProfileComponent implements OnDestroy {
  private unsubscribe$: Subject<boolean> = new Subject();

  public user: UserModel | null = null;
  /**
   * Показывать (true) текст пароля в форме или нет (false)
   */
  public isVisible: boolean = false;
  /**
   * Режим работы компонента:
   * false (по умолчанию) - режим просмотра,
   * true - режим редактирвоания профиля.
   */
  public isEditMode: boolean = false;
  public cities: CityModel[] = [];
  public userDataForm = new FormGroup({
    email: new FormControl({ value: this.user?.email, disabled: true }, [Validators.required, Validators.email]),
    password: new FormControl(this.user?.password, [Validators.required, Validators.minLength(4)]),
    name: new FormControl({ value: this.user?.name, disabled: true }, [Validators.required, Validators.minLength(1)]),
    lastname: new FormControl({ value: this.user?.lastname, disabled: true }),
    bDate: new FormControl<Date | null>({ value: null, disabled: true })
  });

  @ViewChild('passwordInput', { static: false }) passwordInput!: ElementRef;

  constructor(
    private store: Store,
  ) {
    this.store.select(CitiesSelector.all)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(cities => this.cities = structuredClone(cities));

    this.store.select(UserSelector.user)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(user => this.updateUserDataForm(user));
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }

  /**
   * Изменить режим работы.
   * @param value true - режим редактирования, false - режим просмотра.
   */
  public changeMode(value: boolean): void {
    this.isEditMode = value;
    let name = this.userDataForm.get('name');
    let lastname = this.userDataForm.get('lastname');

    if (value) {
      name?.enable({ onlySelf: true });
      lastname?.enable({ onlySelf: true });
      return;
    }

    name?.disable({ onlySelf: true });
    lastname?.disable({ onlySelf: true });
  }

  /**
   * Обновить данные формы.
   * @param userData Данные для формы.
   */
  private updateUserDataForm(userData: UserModel | null): void {
    this.user = structuredClone(userData) ?? new UserModel('');
    this.user.password = '';

    this.userDataForm.patchValue({
      email: this.user.email,
      name: this.user.name,
      lastname: this.user.lastname,
      bDate: !this.user.bDate ? null : new Date(this.user.bDate!)
    });
  }

  /**
   * Сохранить изменения.
   */
  public saveData(): void {
    this.changeMode(false);
    this.user!.name = this.userDataForm.value.name;
    this.user!.lastname = this.userDataForm.value.lastname;
    this.user!.password = this.userDataForm.value.password!;

    let props = new ActionPropsModel(new ItemModel(this.user!.id, this.user), true);
    this.store.dispatch(UserAction.put(props));
  }
}

export const apiOk = '[Global] ApiOk';  // Запрос в БД API сервера успешен и не требует изменений в Store.

export const incorrectUserId = 'Incorrect User ID.';
export const incorrectId = 'Incorrect ID.';
export const emptyResponse = 'Empty API response.';
export const storeDataNotFound = 'Data not found in storage.';
export const accessTokenExpired = 'Access token has expired.';
export const accessTokenName = 'accessToken';

export const ru_accessTokenExpired = 'Срок действия токена доступа истёк.';
export const ru_incorrectData = 'Учётные данные неверны.';
export const ru_serverNotAvailable = 'Сервер недоступен.';
export const ru_addedSuccessfully = 'Данные успешно добавлены.';
export const ru_putedSuccessfully = 'Данные успешно обновлены.';
export const ru_deletedSuccessfully = 'Данные успешно удалены.';

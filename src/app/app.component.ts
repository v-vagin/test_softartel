import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, RouterOutlet } from '@angular/router';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

import { Subject, takeUntil } from 'rxjs';
import { Store } from '@ngrx/store';

import { ApiService } from './services/api.service';

import { HeaderComponent } from './components/header/header.component';
import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';

import { accessTokenExpired } from './consts/global-const';

import { SegmentEnum } from './enums/segment-enum';

import { PageInfoModel } from './models/page-info-model';

import * as PageInfoSelector from './store/page-info/selectors';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterOutlet,
    HeaderComponent,
    BreadcrumbsComponent,
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
  providers: [
  ]
})
export class AppComponent implements OnInit, OnDestroy {
  public readonly segmentEnum = SegmentEnum;

  private unsubscribe$: Subject<void> = new Subject();
  public pageInfo: PageInfoModel | null = null;

  private notificationConfig: MatSnackBarConfig = {
    horizontalPosition: 'end',
    verticalPosition: 'top'
  };

  constructor(
    private router: Router,
    private store: Store,
    private api: ApiService,
    private _snackBar: MatSnackBar,
  ) {
    // Обработка истечения времени действия JWT Токена.
    this.api.authorizationRequired$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        this._snackBar.open(accessTokenExpired, '', this.notificationConfig);
        this.router.navigate([SegmentEnum.Login])
      });

    // ОБработка возникновения ошибок в запросах к API серверу.
    this.api.notification$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((message) => {
        this._snackBar.open(message, '', this.notificationConfig);
      });

    // Обработка смены информации о странице.
    this.store.select(PageInfoSelector.page)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(page => {
        this.pageInfo = structuredClone(page);
      });
  }

  ngOnInit(): void {
    // this.api.initDb(); // Раскомментировать, если нужно создать БД API сервера с нуля при запуске.
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}

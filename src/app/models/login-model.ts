/**
 * Модель данных авторизации пользователя.
 */
export class LoginModel {
  /**
   * Логин пользователя.
   */
  public email: string | null;
  /**
   * Пароль пользователя.
   */
  public password: string | null;

  /**
   * Конструктор модели.
   * @param email Логин пользователя.
   * @param password Пароль пользователя.
   */
  constructor(email: string, password: string) {
    this.email = email;
    this.password = password;
  }
}

import { IdModel } from "./id-model";

/**
 * Интерфейс представления данных для получения списка через Store эффект.
 */
export interface IIdListModel {
  /**
   * true - получить весь список, в этом случае значения свойства list не учитывается.
   */
  all?: boolean | null;
  /**
   * Список ID элементов необходимых для получения, если свойство all != true.
   */
  list?: Array<number> | null;
}

/**
 * Модель данных передаваемых в свойствах Action.
 */
export class IdListModel implements IIdListModel {
  public all?: boolean | null;
  public list?: Array<number> | null;

  /**
   * Конструктор модели.
   * @param all true - получить весь список, в этом случае значения свойства list не учитывается.
   * @param list Список ID элементов необходимых для получения, если свойство all != true.
   */
  constructor(all?: boolean | null, list?: Array<number> | null) {
    this.all = all;
    this.list = list;
  }
}

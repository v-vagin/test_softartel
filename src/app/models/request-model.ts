import { RequestMethodEnum } from "../enums/request-method-enum";

/**
 * Модель запроса данных.
 */
export class RequestModel {
  /**
   * Метод HTTP запроса @see {@link RequestMethodEnum}.
   */
  public method: RequestMethodEnum = RequestMethodEnum.Get;
  /**
   * Данные запроса.
   */
  public data?: any;

  constructor(method: RequestMethodEnum = RequestMethodEnum.Get, data?: any) {
    this.method = method;
    this.data = data;
  }
}

export class ErrorModel {
  public status: number = 0;
  public statusText: string | null = null;

  constructor(status: number, statusText: string | null = null) {
    this.status = status;
    this.statusText = statusText;
  }
}

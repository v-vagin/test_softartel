/**
 * Интерфейс представления "хлебной крошки".
 */
export interface IBreadcrumbModel {
  /**
   * Наименование "хлебной крошки".
   */
  name: string | null;
  /**
   * URL "хлебной крошки".
   */
  url: string | null;
}

/**
 * Модель данных "хлебной крошки".
 */
export class BreadcrumbModel implements IBreadcrumbModel {
  public name: string | null;
  public url: string | null;

  /**
   * Конструктор модели.
   * @param name Наименование "хлебной крошки".
   * @param url URL "хлебной крошки".
   */
  constructor(name: string, url: string | null = null) {
    this.name = name;
    this.url = url;
  }
}

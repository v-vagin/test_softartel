/**
 * Интерфейс представления ID.
 */
export interface IIdModel {
  /**
   * ID элемента данных.
   */
  id: number | null;
}

/**
 * Модель данных ID.
 */
export class IdModel implements IIdModel {
  public id: number | null;

  /**
   * Конструктор модели.
   * @param id ID элемента данных.
   */
  constructor (id: number) {
    this.id = id;
  }
}

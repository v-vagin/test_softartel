/**
 * Интерфейс представления данных для получения/отправки одного элемента через Store Эффект.
 */
export interface IItemModel<T> {
  /**
   * Id для получения/отправки элемента.
   */
  id?: number | null;
  /**
   * Элемент необходимый для получения/отправки.
   */
  item?: T | null;
}

/**
 * Модель данных передаваемых в свойствах Action.
 */
export class ItemModel<T> implements IItemModel<T> {
  public id?: number | null;
  public item?: T | null;

  /**
   * Конструктор модели.
   * @param id Id для получения/отправки элемента.
   * @param item Элемент необходимый для получения/отправки.
   */
  constructor(id?: number | null, item?: T | null) {
    this.id = id;
    this.item = item;
  }
}

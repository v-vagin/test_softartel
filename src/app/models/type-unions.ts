import { CityModel } from "./city-model";
import { TicketModel } from "./ticket-model";
import { UserModel } from "./user-model";

export type ApiListType = CityModel | UserModel | TicketModel;

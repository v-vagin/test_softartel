import { IIdModel } from "./id-model";

/**
 * Интерфейс представления тикета.
 */
export interface ITicketModel extends IIdModel {
  /**
   * Наименование тикета.
   */
  name: string | null;
  /**
   * Дата и время создания тикета в Unix тиках.
   */
  cDate: number | null;
}

/**
 * Модель данных тикета.
 */
export class TicketModel implements ITicketModel {
  /**
   * ID тикета.
   */
  public id: number | null;
  public name: string | null;
  public cDate: number | null;

  /**
   * Конструктор модели.
   * @param id ID тикета.
   * @param name Наименование тикета.
   * @param cDate Дата и время создания тикета.
   */
  constructor(id: number, name: string, cDate?: number | null) {
    this.id = id;
    this.name = name;
    this.cDate = cDate ?? Date.now().valueOf();
  }
}

import { UserModel } from "./user-model";

/**
 * Модель данных ответа API сервера авторизации.
 */
export class AuthResponseModel {
  /**
   * JWT токен.
   */
  public accessToken: string | null;
  /**
   * Данные пользователя.
   */
  public user: UserModel | null;

  /**
   * Конструктор модели.
   * @param accessToken JWT токен.
   * @param user Данные пользователя.
   */
  constructor(accessToken: string, user: UserModel) {
    this.accessToken = accessToken;
    this.user = user;
  }
}

import { IIdModel } from "./id-model";

/**
 * Интерфейс представления города.
 */
export interface ICityModel extends IIdModel {
  /**
   * Наименование города.
   */
  name: string | null;
}

/**
 * Модель данных города.
 */
export class CityModel implements ICityModel {
  /**
   * ID города.
   */
  public id: number | null;
  public name: string | null;

  /**
   * Конструктор модели.
   * @param id ID города.
   * @param name Наименование города.
   */
  constructor(id: number, name: string) {
    this.id = id;
    this.name = name;
  }
}

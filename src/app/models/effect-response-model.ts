/**
 * Интерфейс представления данных в ответе Store Эффекта.
 */
export interface IEffectResponseModel<T> {
  /**
   * Наименование Action для Store Редуктора.
   */
  type: string;
  /**
   * Данные от API сервера.
   */
  data?: T | null;
}

/**
 * Модель данных в свойствах Action.
 */
export class EffectResponseModel<T> implements IEffectResponseModel<T> {
  public type: string = '';
  public data?: T | null = null;

  /**
   * Конструктор модели.
   * @param save Наименование Action для Store Редуктора.
   * @param data Данные от API сервера.
   */
  constructor(type: string, data?: T | null) {
    this.type = type;
    this.data = data;
  }
}

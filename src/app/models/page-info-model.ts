/**
 * Интерфейс представления наименования страницы.
 */
export interface IPageInfoModel {
  /**
   * Сегмент URL пути страницы.
   */
  segment: string | null;
  /**
   * Наименование страницы.
   */
  name: string | null;
  /**
   * URL страницы.
   */
  url: string | null;
}

/**
 * Модель данных наименования страницы.
 */
export class PageInfoModel implements IPageInfoModel {
  public segment: string | null;
  public name: string | null;
  public url: string | null;

  /**
   * Конструктор модели.
   * @param segment Сегмент URL пути страницы.
   * @param name Наименование страницы.
   * @param url URL страницы.
   */
  constructor(segment: string, name: string | null = null, url: string | null = null) {
    this.segment = segment;
    this.name = name;
    this.url = url;
  }
}

/**
 * Интерфейс представления данных в свойствах Action.
 */
export interface IActionPropsModel<T> {
  /**
   * Данные для передачи данных.
   */
  data: T | null;
  /**
   * true - если результат выполнения API запросов (reponse) необходимо сохранять в Store.
   */
  save?: boolean | null;
}

/**
 * Модель данных в свойствах Action.
 */
export class ActionPropsModel<T> implements IActionPropsModel<T> {
  public data: T | null = null;
  public save?: boolean;

  /**
   * Конструктор модели.
   * @param data Передаваемые данные.
   * @param save true - если результат выполнения API запроса (response) необходимо также сохранить в Store.
   */
  constructor(data: T | null, save: boolean = true) {
    this.data = data;
    this.save = save;
  }
}

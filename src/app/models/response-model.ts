/**
 * Модель ответа API сервера.
 */
export class ResponseModel<T> {
  /**
   * Статус запроса: true (по умолчанию) - запрос выполнен успешно, без ошибок.
   */
  public ok: boolean = true;
  /**
   * Содержит описание ошибки при её возникновении, либо null.
   */
  public status?: string | null;
  /**
   * Данные, либо null, полученные в результате выполнения запроса.
   */
  public data?: T | null;

  /**
   * Конструктор модели.
   * @param ok Статус запроса: true (по умолчанию) - запрос выполнен успешно, без ошибок.
   * @param data Данные, либо null, полученные в результате выполнения запроса.
   * @param status Содержит описание ошибки при её возникновении, либо null.
   */
  constructor(ok: boolean = true, data?: T | null, status?: string | null) {
    this.ok = ok;
    this.status = status;
    this.data = data;
  }
}

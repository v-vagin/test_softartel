import { IIdModel } from "./id-model";

/**
 * Интерфейс представления пользователя.
 */
export interface IUserModel extends IIdModel {
  /**
   * Email (login) пользователя.
   */
  email: string | null;
  /**
   * Пароль пользователя.
   */
  password: string | null;
  /**
   * Имя пользователя.
   */
  name?: string | null;
  /**
   * Фамилия пользователя.
   */
  lastname?: string | null;
  /**
   * Дата рождения пользователя в Unix тиках.
   */
  bDate?: number | null;
  /**
   * ID города пользователя.
   */
  cityId?: number | null;
  /**
   * Список id тикетов пользователя.
   */
  tickets?: Array<number> | null;
}

/**
 * Модель данных пользователя.
 */
export class UserModel implements IUserModel {
  /**
   * ID пользователя.
   */
  public id: number | null = null;
  public email: string | null;
  public password: string | null = null;
  public name?: string | null;
  public lastname?: string | null;
  public bDate?: number | null;
  public cityId?: number | null;
  public tickets?: Array<number> | null;

  /**
   * Конструктор модели.
   * @param id ID пользователя.
   * @param email Email (login) пользователя.
   * @param password Пароль пользователя.
   * @param name Имя пользователя.
   * @param lastname Фамилия пользователя.
   * @param bDate Дата рождения пользователя в Unix тиках.
   * @param cityId ID города пользователя.
   * @param tickets Список id тикетов пользователя.
   */
  constructor(email: string | null, name?: string | null, lastname?: string | null,
    bDate?: number | null, cityId?: number | null, tickets?: Array<number> | null
  ) {
    this.email = email;
    this.name = name;
    this.lastname = lastname;
    this.bDate = bDate?.valueOf();
    this.cityId = cityId;
    this.tickets = tickets;
  }
}

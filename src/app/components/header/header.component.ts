import { Component, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, RouterModule } from '@angular/router';

import { Subject, takeUntil } from 'rxjs';
import { Store } from '@ngrx/store';

import { ApiService } from '../../services/api.service';

import { SegmentEnum } from '../../enums/segment-enum';
import { SegmentNameEnum } from '../../enums/segment-name-enum';

import { PageInfoModel } from '../../models/page-info-model';
import { UserModel } from '../../models/user-model';

import * as PageInfoSelector from '../../store/page-info/selectors';
import * as UserSelector from '../../store/user/selectors';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [
    CommonModule,
    RouterModule,
  ],
  templateUrl: './header.component.html',
  styleUrl: './header.component.scss'
})
export class HeaderComponent implements OnDestroy {
  public readonly segmentEnum = SegmentEnum;
  public readonly segmentNameEnum = SegmentNameEnum;

  private unsubscribe$: Subject<void> = new Subject();
  public pageInfo: PageInfoModel | null = null;
  public user: UserModel | null = null;

  public isVisible: boolean = true;
  public hiddenSegments: Array<string> = [
    this.segmentEnum.PageNotFound,
    this.segmentEnum.DataNotFound
  ];

  constructor(
    private router: Router,
    private api: ApiService,
    private store: Store,
  ) {
    this.store.select(PageInfoSelector.page)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(x => {
        this.pageInfo = structuredClone(x);
        this.isVisible = !this.hiddenSegments.includes(this.pageInfo?.segment ?? '')
      });

    this.store.select(UserSelector.user)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(x => this.user = structuredClone(x));
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public doLogout(): void {
    this.api.logout();
    this.router.navigate([SegmentEnum.Login], { replaceUrl: true });
  }
}

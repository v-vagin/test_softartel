import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { Store } from '@ngrx/store';

import { BreadcrumbModel } from '../models/breadcrumb-model';
import { PageInfoModel } from '../models/page-info-model';

import * as PageInfoAction from '../store/page-info/actions';
import * as BreadcrumbsAction from "../store/breadcrumbs/actions";
import * as CitiesAction from '../store/cities/actions';
import * as TicketsAction from '../store/tickets/actions';
import * as UsersAction from '../store/users/actions';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  constructor(
    private store: Store,
    private titleService: Title,
  ) {
  }

  /**
   * Инициализация Store.
   */
  public initStore(): void {
    this.store.dispatch(BreadcrumbsAction.clear());
    this.store.dispatch(PageInfoAction.clear());
    this.store.dispatch(CitiesAction.clear());
    this.store.dispatch(UsersAction.clear());
    this.store.dispatch(TicketsAction.clear());
  }

  /**
   * Установка заголовка текущией страницы и обновление pageInfo текущей страницы в Store.
   * @param name Наименование страницы.
   * @param segment Сегмент URL страницы.
   */
  public setPageTitle(name: string, segment: string | null = null): void {
    this.titleService.setTitle(!name ? '' : name);
    this.store.dispatch(PageInfoAction.put({ item: new PageInfoModel(segment ?? '', name) }));
  }

  /**
   * Установка "хлебных крошек" страницы.
   * @param list Список "хлебных крошек".
   */
  public setPageBreadcrumbs(list: Array<BreadcrumbModel>) {
    this.store.dispatch(BreadcrumbsAction.put({ list }));
  }

}

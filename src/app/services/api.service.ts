import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { firstValueFrom, Observable, of, Subject } from 'rxjs';
import { Store } from '@ngrx/store';
import { jwtDecode, JwtPayload } from 'jwt-decode';

import { isIdCorrect, randomInt } from './helper.service';

import { accessTokenExpired, accessTokenName, emptyResponse, incorrectUserId } from '../consts/global-const';

import { ApiListTypeEnum } from '../enums/api-list-type-enum';
import { SegmentEnum } from '../enums/segment-enum';
import { RequestMethodEnum } from '../enums/request-method-enum';

import { UserModel } from '../models/user-model';
import { LoginModel } from '../models/login-model';
import { AuthResponseModel } from '../models/auth-response-model';
import { CityModel } from '../models/city-model';
import { TicketModel } from '../models/ticket-model';
import { RequestModel } from '../models/request-model';
import { ActionPropsModel } from '../models/action-props-model';

import * as UserAction from '../store/user/actions';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiServer: string = 'http://localhost:3000/api';
  private authHeader: any = null;
  private accessToken?: string | null;
  private accessTokenExp: number = 0;

  public authorizationRequired$: Subject<boolean> = new Subject();
  public notification$: Subject<string> = new Subject();

  constructor(
    private store: Store,
    private http: HttpClient,
  ) {
  }

  //#region Функции для авторизации

  /**
   * Установить заголовок авторизации.
   * @param token JWT токен, либо null для сброса заголовкf авторизации/
   */
  public setAuthHeader(token?: string | null): void {
    if ((token?.trim() ?? '') == '') {
      this.authHeader = null;
      return;
    }

    this.authHeader = { 'Authorization': `Bearer ${token}` };
  }

  /**
   * Деавторизация пользователя.
   */
  public logout(): void {
    this.accessTokenExp = 0;
    this.setAuthHeader();
    this.accessToken = null;
    this.store.dispatch(UserAction.putOk(new ActionPropsModel<UserModel>(null)));
    localStorage.removeItem(accessTokenName);
  }

  /**
   * Авторизация пользователя.
   * @param loginData Данные для авторизации.
   * @returns ID пользователя при успешной авторизации, либо 0.
   */
  public async login(loginData: LoginModel): Promise<number> {
    this.logout();

    try {
      let email = loginData?.email?.trim() ?? '';
      let password = loginData?.password?.trim() ?? '';

      if (!email || !password) {
        throw 'Email or password is empty.';
      }

      let url: string = `${this.apiServer}/${SegmentEnum.Login}`;
      let response = await firstValueFrom(this.http.post<AuthResponseModel>(url, loginData));

      if (!response) {
        throw emptyResponse;
      }

      let userId = response.user?.id ?? 0;
      let token = response.accessToken?.trim() ?? '';

      if (!isIdCorrect(userId) || !token) {
        throw incorrectUserId;
      }

      let decoded: JwtPayload = jwtDecode(token);
      this.accessTokenExp = decoded?.exp ?? 0;

      if (this.isAccessTokenExpired()) {
        throw accessTokenExpired;
      }

      let decodedId = parseInt(decoded.sub ?? '');

      if (!isIdCorrect(decodedId) || decodedId != userId) {
        throw incorrectUserId;
      }

      localStorage.setItem(accessTokenName, token);
      this.setAuthHeader(token);

      this.accessToken = `${token}`;
      this.store.dispatch(UserAction.putOk(new ActionPropsModel<UserModel>(response.user)));
      return userId;

    } catch (error) {
      switch (true) {
        case error instanceof HttpErrorResponse:
          console.warn(error.statusText);
          if (error.status == 0) {
            // Нет связи с сервером следовательно невозможно авторизовать пользователя.
            return -1;
          }
          break;

        case error instanceof Error:
          console.warn(error.message);
          break;

        default:
          console.warn(error);
          break;
      }
      return 0;
    }
}

  /**
   * Проверка времени действия токена.
   * @returns true - если время действия токена истекло.
   */
  private isAccessTokenExpired = () =>
    Math.round(Date.now() / 1000) >= this.accessTokenExp;

  /**
   * Проверка токена доступа на актуальность.
   * @returns ID пользователя при актуальном токене доступа, либо 0.
   */
  public async checkAccessToken(): Promise<number> {
    let token = localStorage.getItem(accessTokenName)?.trim() ?? '';

    if (!token) {
      this.logout();
      return 0;
    }

    try {
      let decoded: JwtPayload = jwtDecode(token);
      this.accessTokenExp = decoded?.exp ?? 0;

      if (this.isAccessTokenExpired()) {
        throw new Error(accessTokenExpired, { cause: 401 });
      }

      let decodedId = parseInt(decoded.sub ?? '');

      if (!isIdCorrect(decodedId)) {
        throw new Error(incorrectUserId, { cause: 401 });
      }

      // Проверка действительности токена через запрос информации о пользователе.
      this.setAuthHeader(token);
      let url: string = `${this.apiServer}/${SegmentEnum.Users}/${decodedId}`;
      let response = await firstValueFrom(this.http.get<UserModel>(url, { headers: this.authHeader }));
      let userId = response?.id ?? 0;

      if (!isIdCorrect(userId) || userId != decodedId) {
        throw new Error(incorrectUserId, { cause: 401 });
      }

      this.accessToken = `${token}`;
      this.store.dispatch(UserAction.putOk(new ActionPropsModel<UserModel>(response)));
      return userId;

    } catch (error) {
      switch (true) {
        case error instanceof HttpErrorResponse:
          console.warn(error.statusText);
          if (error.status == 0) {
            // Нет связи с сервером следовательно невозможно проверить токен,
            // поэтому принудительынй logout не делаем, во избежание пренаправления на страницу авторизации.
            return -1;
          }
          break;

        case error instanceof Error:
          console.warn(error.message);
          break;

        default:
          console.warn(error);
          break;
      }
      this.logout();
      return 0;
    }
  }

  /**
   * Проверка права доступа к запрашиваемой странице на основе токена доступа.
   * @returns true - если доступ позволен.
   */
  public async checkAccessRights() {
    let id = await this.checkAccessToken();

    if (id < 0 || id > 0) {
      // Пользователь считается авторизованным, если:
      // - ID > 0 - токен проверен
      // - ID < 0 - токен не проверен из-за отсутствия связи с сервером
      //   принудительынй logout не сделан, во избежание пренаправления на страницу авторизации.
      return true;
    }

    this.authorizationRequired$.next(true);
    return false;
  };

  //#endregion


  //#region API функции

  public getUrl(listType: ApiListTypeEnum, id?: number | null): string {
    let segment: string = '';
    switch (listType) {
      case ApiListTypeEnum.Cities: segment = `/${SegmentEnum.Cities}`; break;
      case ApiListTypeEnum.Users: segment = `/${SegmentEnum.Users}`; break;
      case ApiListTypeEnum.Tickets: segment = `/${SegmentEnum.Tickets}`; break;
      case ApiListTypeEnum.City: segment = `/${SegmentEnum.City}`; break;
      case ApiListTypeEnum.User: segment = `/${SegmentEnum.User}`; break;
      case ApiListTypeEnum.Ticket: segment = `/${SegmentEnum.Ticket}`; break;
    }
    let idSegment = !isIdCorrect(id) ? '' : `/${id}`;
    return `${this.apiServer}${segment}${idSegment}`;
  }

  public request<T>(url: string, request: RequestModel): Observable<T> {
    if (this.isAccessTokenExpired()) {
      console.warn(accessTokenExpired);
      this.logout()
      this.authorizationRequired$.next(true);
      return of()
    }

    switch (request.method) {
      case RequestMethodEnum.Post:
        return this.http.post<T>(url, request.data, { headers: this.authHeader });

      case RequestMethodEnum.Put:
        return this.http.put<T>(url, request.data, { headers: this.authHeader });

      case RequestMethodEnum.Delete:
        return this.http.delete<T>(url, { headers: this.authHeader });
    }

    return this.http.get<T>(url, { headers: this.authHeader });
  }

  /**
   * Инициализация БД API сервера.
   * @returns
   */
  public async initDb(): Promise<void> {
//#region Список городов
    let cityNames: Array<string> = [
      'Москва', 'Санкт-Петербург', 'Новосибирск', 'Екатеринбург', 'Нижний Новгород', 'Самара',
      'Омск', 'Казань', 'Челябинск', 'Ростов-на-Дону', 'Уфа', 'Волгоград'
    ];
    let cities: Array<CityModel> = [];
    let count = cityNames.length;
    for (let i = 0; i < count; i++) {
      cities.push({id: i + 1, name: cityNames[i]});
    }
//#endregion

//#region Список тикетов
    let ticketNames: Array<string> = [
      'Тикет 1 (Иванов)', 'Тикет 2 (Иванов)', 'Тикет 3 (Иванов)',
      'Тикет 4 (Петров)', 'Тикет 5 (Петров)', 'Тикет 6 (Петров)',
      'Тикет 7 (Сидоров)', 'Тикет 8 (Сидоров)', 'Тикет 9 (Сидоров)'
    ];
    let tickets: Array<TicketModel> = [];
    count = ticketNames.length;
    for (let i = 0; i < count; i++) {
      tickets.push({
        id: i + 1,
        name: ticketNames[i],
        cDate: new Date(2024, 4, randomInt(30), randomInt(23, 1), randomInt(59), randomInt(59)).valueOf()
      });
    }
//#endregion

//#region Список пользователей
    let users: Array<UserModel> = [];
    let id: number = 1;
    users.push({
      id: id,
      name: 'Admin',
      bDate: new Date(1972, 6, 3).valueOf(),
      cityId: 12,
      tickets: null,
      email: 'admin@mail.ru',
      password: '$2a$10$lXYBTF6rLsASrNjgkpfyS..wn.JTgAkzfVUA9Mg8R/SgRoFvyij1y' // admin
    });

    id++;
    users.push({
      id: id,
      name: 'Иван',
      lastname: 'Иванов',
      bDate: new Date(1972, 6, 3).valueOf(),
      cityId: 1,
      tickets: [1, 2, 3],
      email: 'ivan@mail.ru',
      password: '$2a$10$LPf5jkzvngcPD0sCX01BGOnEudEuMKdYtIhGGA1AdwE5nxa8YH8qG' // ivan
    });

    id++;
    users.push({
      id: id,
      name: 'Пётр',
      lastname: 'Петров',
      bDate: new Date(1973, 7, 4).valueOf(),
      cityId: 2,
      tickets: [4, 5, 6],
      email: 'petr@mail.ru',
      password: '$2a$10$E.opYgiwIuvD/Ml44uvfCO3qz2T05BdxaS2xzsHkLLTFSEjhIYNM2' // petr
    });

    id++;
    users.push({
      id: id,
      name: 'Сидор',
      lastname: 'Сидоров',
      bDate: new Date(1974, 8, 5).valueOf(),
      cityId: 3,
      tickets: [7, 8, 9],
      email: 'sidor@mail.ru',
      password: '$2a$10$fkyPE1/./QtHSvFDR4ZUUuZYVAVnvLpI3KzoyCZrM81MCjuheZiPy' // sidor
    });
//#endregion

    let data = {
      "cities": cities,
      "tickets": tickets,
      "users": users
    };

    let request: string = `${this.apiServer}/reset`;

    // т.к. json-server-reset некорректно возвращает статус операции,
    // то выполняем запрос в блоке try-catch
    try {
      return await firstValueFrom(this.http.post<any>(request, data));
    } catch (e) {
      return;
    }
  }

}

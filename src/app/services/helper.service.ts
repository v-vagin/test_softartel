import { HttpErrorResponse } from '@angular/common/http';
import { UrlSegment } from '@angular/router';
import { debounceTime, distinctUntilChanged, fromEvent, map, switchMap, tap } from 'rxjs';
import { ErrorModel } from '../models/error-model';

/**
 * Проверка значения ID на правильность: ID должно быть целым,
 * положительным и не нулевым числом.
 * @param id значение ID для првоерки.
 * @returns true - если ID целое, положительное и не нулевое число.
 */
export const isIdCorrect = (id: any): boolean => {
  if (!id || id < 0 || (id % 1) !== 0) {
    return false;
  }

  return true;
}

/**
 * Генерация GUID.
 * @returns GUID
 */
export const guid = (): string => {
  let guid: string;
  try {
    guid = crypto.randomUUID()
  } catch (error) {
    guid = "10000000-1000-4000-8000-100000000000".replace(/[018]/g, c =>
      (+c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> +c / 4).toString(16)
    );
  }
  return guid;
}

/**
 * Получить числовое значение ID из последнего сегмента URL.
 * @param segments Массив {@link UrlSegment}.
 * @returns Значение ID, либо -1 при неверном значении ID.
 */
export const getId = (segments: UrlSegment[]): any => {
  let length = segments?.length ?? 0;

  if (length < 2) {
    return -1;
  }

  let id: number = parseInt(segments[length - 1].path);

  if (!isIdCorrect(id)) {
    return -1;
  }

  return id;
}

/**
 * Организация задержки в миллисекундах.
 * @param msec Задержка в миллисекундах.
 * По умолчанию 500 миллисекунд.
 */
export const delay = async (msec: number = 500): Promise<void> => {
  return new Promise(r => setTimeout(r, msec));
}

/**
 * Получить целое случайное число в заданном диапазоне.
 * @param hiLimit Верхний предел. 1 - по умолчанию.
 * @param lowLimit Нижний предел. 0 - по умочланию.
 * @returns Случайное число в заданном диапазоне.
 */
export const randomInt = (hiLimit: number = 1, lowLimit: number = 0) => {
  return Math.floor(Math.random() * hiLimit + lowLimit);
}

/**
 * Получить дату в формате {@link Date}.
 * @param value Значение даты одном из форматов: {@link Number} или {@link Date}.
 * Если значение даты не определено, то возвращается текущая дата.
 * @returns Дата в формате {@link Date}.
 */
const getDate = (value?: Date | number | null): Date => {
  return !value
    ? new Date(Date.now())
    : value instanceof Date
      ? value
      : new Date(value as number);
}
/**
 * Получить значения даты и времени в строковом формате: DD-MM-YYYY HH:MM:SS.
 * @param value Значение даты в одном из форматов: {@link Number} или {@link Date}.
 * Если значение даты не определено, то возвращается текущая дата.
 * @returns Дата в строковом формате: DD-MM-YYYY HH:MM:SS.
 */
export const dateTimeToString = (value?: Date | number | null): string => {
  let d = getDate(value);
  let year = `${d.getUTCFullYear()}`.padStart(4, '0');
  let month = `${d.getUTCMonth() + 1}`.padStart(2, '0');
  let date = `${d.getUTCDate()}`.padStart(2, '0');
  let hour = `${d.getUTCHours()}`.padStart(2, '0');
  let min = `${d.getMinutes()}`.padStart(2, '0');
  let sec = `${d.getSeconds()}`.padStart(2, '0');
  return `${date}-${month}-${year} ${hour}:${min}:${sec}`;
}

/**
 * Получить значения даты и времени в строковом формате: DD-MM-YYYY.
 * @param value Значение даты в одном из форматов: {@link Number} или {@link Date}.
 * Если значение даты не определено, то возвращается текущая дата.
 * @returns Дата в строковом формате: DD-MM-YYYY.
 */
export const dateToString = (value?: Date | number | null): string => {
  let d = getDate(value);
  let year = `${d.getUTCFullYear()}`.padStart(4, '0');
  let month = `${d.getUTCMonth() + 1}`.padStart(2, '0');
  let date = `${d.getUTCDate()}`.padStart(2, '0');
  return `${date}-${month}-${year}`;
}

/**
 * Получить значения времени в строковом формате: HH:MM:SS.
 * @param value Значение даты в одном из форматов: {@link Number} или {@link Date}.
 * Если значение даты не определено, то возвращается текущая дата.
 * @returns Время в строковом формате: HH:MM:SS.
 */
export const timeToString = (value?: Date | number | null): string => {
  let d = getDate(value);
  let hour = `${d.getUTCHours()}`.padStart(2, '0');
  let min = `${d.getMinutes()}`.padStart(2, '0');
  let sec = `${d.getSeconds()}`.padStart(2, '0');
  return `${hour}:${min}:${sec}`;
}

/**
 * Быстрое (рекурсивное) копирование объекта, аналог structuredClone().
 * @param src Объект для копирования.
 * @returns Копия объекта.
 */
export const deepCopy = (src: any): any => {
  if (typeof src !== 'object' || src === null) {
    return src;
  }

  if (src instanceof Date) {
    return new Date(src.getTime());
  }

  if (src instanceof Array) {
    return src.reduce((arr, item, i) => {
      arr[i] = deepCopy(item);
      return arr;
    }, []);
  }

  if (src instanceof Object) {
    return Object.keys(src).reduce((newObj: any, key: string) => {
      newObj[key] = deepCopy(src[key]);
      return newObj;
    }, {})
  }
}

/**
 * Копирование объекта через сериализацию/де-сериализацию.
 * @param src Объект для копирования.
 * @returns Копия объекта.
 */
export const deepJCopy = (src: any) => {
  return JSON.parse(JSON.stringify(src));
}

/**
 * Ввод текста с задержкой в поисковой строке с обращением к API серверу и запупуском спиннера.
 * @param input {@link HTMLInputElement}
 * @param fetchDataCallback Callback Observable функция с обращением к API функции.
 * @param spinnerCallback Callback функция с запуском/остановкой спиннера.
 */
export const inputDelay = (input: HTMLInputElement, fetchDataCallback: any, spinnerCallback: any) => {
  return fromEvent(input, 'input').pipe(
    map((event: any) => event?.target?.value),
    debounceTime(500),
    distinctUntilChanged(),
    tap(() => spinnerCallback(true)), // Запуск спинера
    switchMap(query => fetchDataCallback(query)), // API запрос
    tap(() => spinnerCallback(false)), // Остановка спинера
  );
}

/**
 * Получить значения ошибки в строковом формате: 'КОД - СТАТУС'.
 * @param error Информация об ошибке.
 * @param isWarnToConsole true (по умолчанию) - если нужно дублировать строку ошибки на консоль.
 * @returns Ошибка в строковом формате: 'КОД - СТАТУС'.
 */
export const errorToString = (error: any, isWarnToConsole = true): string => {
  let message: string;

  switch (true) {
    case error instanceof HttpErrorResponse:
      message = `${error.status} - ${error.statusText}`;
      break;

    case error instanceof Error:
      let cause = !error.cause ? '' : `${error.cause} - `;
      message = `${cause}${error.message}`;
      break;

    default:
      message = `${error.message}`;
      break;
  }

  if (isWarnToConsole) {
    console.warn(message);
  }

  return message;
}

export const parseError = (error: any): ErrorModel => {
  switch (true) {
    case error instanceof HttpErrorResponse:
      return new ErrorModel(error?.status ?? 0, error?.statusText ?? null);

    case error instanceof Error:
      let cause = parseInt(`${error?.cause ?? ''}`);
      let value = isNaN(cause) ? 0 : cause;
      return new ErrorModel(value, error?.message);

    default:
      return new ErrorModel(0, `${error}`);
  }
}

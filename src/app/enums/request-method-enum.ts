export enum RequestMethodEnum {
  Get = 1,
  Head,
  Post,
  Put,
  Delete,
  Connect,
  Options,
  Trace,
  Patch
}

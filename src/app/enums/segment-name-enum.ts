export enum SegmentNameEnum {
  // frontend
  Main = 'Главная',
  Login = 'Авторизация',
  Profile = 'Профиль',
  Ticket = 'Тикет',
  Tickets = 'Тикеты',
  Dashboard = 'Админка',
  PageNotFound = 'Страница не найдена',
  DataNotFound = 'Данные не найдены',
  ServerNotAvailable = 'Сервер недоступен',

  // API
  Cities = 'Города',
  Users = 'Пользователи',
  City = 'Город',
  User = 'Пользователь',
}

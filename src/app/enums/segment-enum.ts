export enum SegmentEnum {
  // frontend
  Main = 'index',
  Login = 'login',
  Profile = 'profile',
  Ticket = 'ticket',
  Tickets = 'tickets',
  Dashboard = 'dashboard',
  PageNotFound = 'page-not-found',
  DataNotFound = 'data-not-found',
  ServerNotAvailable = 'server-not-available',

  // API
  Cities = 'cities',
  Users = 'users',
  City = 'city',
  User = 'user',
}

export enum ApiListTypeEnum {
  Cities,
  Users,
  Tickets,
  City,
  User,
  Ticket,
}

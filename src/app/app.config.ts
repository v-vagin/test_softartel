import { ApplicationConfig, isDevMode } from '@angular/core';
import { provideRouter } from '@angular/router';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { provideHttpClient } from '@angular/common/http';

import { provideStore } from '@ngrx/store';
import { NavigationActionTiming, provideRouterStore, RouterState } from '@ngrx/router-store';
import { provideEffects } from '@ngrx/effects';
import { provideStoreDevtools } from '@ngrx/store-devtools';

import { routes } from './app.routes';
import { reducers, metaReducers } from './store';

import { CitiesEffects } from './store/cities/effects';
import { TicketsEffects } from './store/tickets/effects';
import { UsersEffects } from './store/users/effects';
import { UserEffects } from './store/user/effects';

import { AppRouterSerializer } from './store/router/states';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';

export const appConfig: ApplicationConfig = {
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'ru' },
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 2500} },
    provideRouter(routes),
    provideAnimationsAsync(),
    provideHttpClient(),

    provideStore(reducers, { metaReducers: metaReducers }),
    provideEffects(
      CitiesEffects,
      TicketsEffects,
      UsersEffects,
      UserEffects,
    ),
    provideRouterStore({
      serializer: AppRouterSerializer,
      routerState: RouterState.Full,
      navigationActionTiming: NavigationActionTiming.PostActivation,
    }),
    provideStoreDevtools({ maxAge: 25, logOnly: !isDevMode()}),
  ]
};
